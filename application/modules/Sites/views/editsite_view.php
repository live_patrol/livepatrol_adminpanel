<style>
	.border-green{
		border: 6px solid green;
	}
	.border-red{
		border: 6px solid red;
	}
	.loading{
		background-image: url("../assets/images/default.gif");
		background-repeat: no-repeat;
	    background-attachment: fixed;
	    background-position: center; 
	}
	.opa{
		opacity : 0;
	}
</style>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-select.js"></script>
<div class="text-center" style="padding:50px 0" id="to-hide" >
	<div class="logo">edit site</div>
	<!-- Main Form -->
        	<?php 
        		foreach ($site->result() as $row) {  ?>
        		<div class="main-login-form">
					<div class="container-fluid">
						<form id ="site-form">
							<input type="hidden" id="siteId" name="siteId" value="<?php echo $row->id; ?>" />
							<div class="form-group col-lg-8">
		                        <input type="text" class="form-control" id="sitename" name="sitename" placeholder="Site Name" value="<?php echo $row->site_name; ?>" required />
							</div>
							<div class="col-lg-4">
									<select class="selectpicker show-tick" name="company" id="company" data-live-search="true" style="width:100%">
									<?php
									foreach ($clients->result() as $row1) { 
											if($row->company == $row1->id){ ?>
												<option><?php echo $row1->firstname ?></option>
											<?php }
										?>

										<option value="<?php echo $row1->id; ?>" <?php if($row->company == $row1->id){ ?> selected <?php } ?> ><?php echo $row1->firstname; ?></option>
									<?php }
									?>
		                            </select>
							</div>
							<span id="edit-site" onclick="editSite(this)" class="btn btn-default" style="width:50%">Edit site</span>
						</form>

						<div id = "cameraDiv" class = "container-fluid col-lg-12">
							<?php 
								$cameraCount = count($cameras->result());
								if(count($cameras->result()) > 0){
									foreach ($cameras->result() as $row2) { ?>
									<div class="row">
										<h3>camera <?php echo $cameraCount; ?></h3>
									</div>
									<span class="fa fa-2x pull-right fa-minus"></span>
									<form>
										<input type="hidden" name="cameraId" value="<?php echo $row2->id; ?>"  >
										<div class="form-group col-lg-12">
											<input type="hidden" id = "cameraCounter" name="cameraCounter" value="<?php echo $cameraCount; ?>">
											<div class="form-group col-lg-8">
												<label>Camera Public URL</label>
												<input type="text" class="form-control" id="rtsp_url<?php echo $cameraCount; ?>" name="rtsp_url" placeholder="camera rtsp url"  value="<?php echo $row2->rtsp_url; ?>" required />
											</div>
											<div class="form-group col-lg-4">
												<label>Camera HTTP Port</label>
												<input type="text" class="form-control" id="rtsp_port<?php echo $cameraCount; ?>" name="rtsp_port" placeholder="camera http port" value="<?php echo $row2->rtsp_port; ?>" required />
											</div>
											<div class="form-group col-lg-6">
												<label>Camera Username</label>
												<input type="text" class="form-control" id="camera_username<?php echo $cameraCount; ?>" name="camera_username" placeholder="camera Username" required value="<?php echo $row2->camera_username; ?>" />
											</div>
											<div class="form-group col-lg-6">
												<label>Camera RTSP Password</label>
												<input type="text" class="form-control" id="camera_password<?php echo $cameraCount; ?>" name="camera_password" placeholder="camera password" required value="<?php echo $row2->camera_password; ?>" />
											</div>
											<div class = "col-lg-12">
												<span class = "btn btn-default" onclick = "testCamera(this, <?php echo $cameraCount; ?>)" >Test Camera URL</span>
											</div>
										</div><br><br>
										<span onclick="deleteCamera(this)" camera="<?php echo $row2->id; ?>" class = "btn btn-danger pull-right"> delete Camera</span>
										<span onclick="editCamera(this)" class = "camera-edit btn btn-default pull-right" disabled> edit Camera</span>
									</form>
								<?php }
								}
							?>
							
						</div>
						<span id = "addCamera" class="btn btn-default" style = "width:50%" >add camera</span>
					</div><br>
				</div>
        	<?php }
        	?>
	<!-- end:Main Form -->
</div>


<script>
		$(document).ready(function(){
			$('.selectpicker').selectpicker();
			$("#company").addClass("hidden");
			$("#username").focusout(function(){
	            var obj = {
	                username : $("#username").val()
	            }
	            $.ajax({
	                type: 'POST',
	                url: '<?php echo base_url(); ?>Client/checkUsername',
	                data: obj,
	                success: function(response) {
	                   if(response == "true"){
	                       $("#username").val("");
	                       $("#user_check").html("Username already exists. Please choose a different one.");
	                   }else{
	                       
	                   }
	                   
	                },
	                error: function(){
	                    alert("internal error");
	                }
	            });
	        });
                    
            function isEveryInputEmpty() {
                    var allEmpty = false;
                    $(':input').each(function() {
                        if ($(this).val() !== '') {
                            allEmpty = false;
                            return false; // we've found a non-empty one, so stop iterating
                        }
                    });

                    return allEmpty;
                }
			var cameraCounter = '<?php echo $cameraCount; ?>';
			var siteCounter = 0;

			$(document).on("click" , ".btn" , function(){
                if((isEveryInputEmpty())){
                    alert("Please fill in all feilds");
                }else{
                	// var site = $(this).attr("site");
                    if($(this).attr("id") == "addCamera"){
						cameraCounter++;
						var col = '<div class="col-lg-12"><h3>camera '+cameraCounter+'</h3></div><span class="fa fa-2x pull-right fa-minus"></span><form><input type="hidden" name="siteId" value="'+$("#siteId").val()+'"><div class="form-group col-lg-12"><input type="hidden" id = "cameraCounter" name="cameraCounter" value="'+cameraCounter+'"><div class="form-group col-lg-8"><label>Camera Public URL</label><input type="text" class="form-control" id="rtsp_url'+cameraCounter+'" name="rtsp_url" placeholder="camera rtsp url" required /></div><div class="form-group col-lg-4"><label>Camera HTTP Port</label><input type="text" class="form-control" id="rtsp_port'+cameraCounter+'" name="rtsp_port" placeholder="camera http port" required /></div><div class="form-group col-lg-6"><label>Camera Username</label><input type="text" class="form-control" id="camera_username'+cameraCounter+'" name="camera_username" placeholder="camera Username" required /></div><div class="form-group col-lg-6"><label>Camera RTSP Password</label><input type="text" class="form-control" id="camera_password'+cameraCounter+'" name="camera_password" placeholder="camera password" required /></div><div class = "col-lg-12"><span class = "btn btn-default" onclick = "testCamera(this, '+cameraCounter+')" >Test Camera URL</span></div></div><br><br><span onclick="submitCamera(this)" class = "camera-edit btn btn-default pull-right" disabled> submit Camera</span></form>';
						$("#cameraDiv").append(col);
						$("#addCamera").attr("disabled" , "true");
						$(this).attr("camera", cameraCounter);
					}else if($(this).attr("id") == "addSite"){
						cameraCounter = 0;
						siteCounter++;
						var col = '<h3>Site '+siteCounter+'</h3><input type="hidden" name="siteCounter" value="'+siteCounter+'"><span class="fa fa-2x pull-right fa-minus"></span><div class="container-fluid"><div class="form-group col-lg-12"><div class="form-group col-lg-12"><label>Sitename</label><input type="text" class="form-control" id="site_name'+siteCounter+'" name="site_name'+siteCounter+'" placeholder="Sitename" required /></div></div><div id = "cameraDiv'+siteCounter+'" class = "container-fluid"></div><span site = "'+siteCounter+'" camera = "0" id = "addCamera'+siteCounter+'" class="btn btn-default" style = "width:100%" >add camera</span><br></div><br><br>';
						$("#siteDiv").append(col);

						// $("#addSite").attr("disabled" , "true");
					}
				}
			});
			$(document).on("click" , ".fa" , function(){
				if($(this).attr("class") === "fa fa-2x pull-right fa-minus"){
					$(this).removeClass("fa-minus");
					$(this).addClass("fa-plus");
					$(this).next("div").addClass("hidden");
				}else if ($(this).attr("class") === "fa fa-2x pull-right fa-plus"){
					$(this).removeClass("fa-plus");
					$(this).addClass("fa-minus");
					$(this).next("div").removeClass("hidden");
				}
			});
            $("#submitSite").click(function(){
                $("textarea").each(function(){
                    this.value = this.value.replace(/\r?\n/g, '<br />');
                });
                $("#add-site-form").submit();
            });
            $(document).on("change" , ".lang-select" , function(){
                if($(this).val() == 1){
                    $(this).parents("div").find("textarea").addClass("text-right");
                }else{
                    $(this).parents("div").find("textarea").removeClass("text-right");
                }
            });
		});

		function testCamera(el, count){
			$(el).parents().find(".camera-edit").removeAttr("disabled");
			// alert("URL : " + $("#rtsp_url"+count).val());
			// alert("Username : " + $("#camera_username"+count).val());
			// alert("Password : " + $("#camera_password"+count).val());
			// alert("Port : " + $("#rtsp_port"+count).val());
			
			// alert($("#rtsp_port"+count).val());
			$("body").addClass("loading");
			$("#to-hide").addClass("opa");			

			// $("#addCamera").attr("disabled" , "true");
			$.ajax({
			    url: 'http://54.190.254.95:8080/checkCameraUrl',
			    type: 'post',
			    data: JSON.stringify({"cameraHost":$("#rtsp_url"+count).val(), "cameraUsername":$("#camera_username"+count).val(), "cameraPassword":$("#camera_password"+count).val(), "cameraPort":$("#rtsp_port"+count).val()}),
				contentType: "application/json;charset=utf-8",
			    dataType: 'json',
			    success: function (data) {
			    	$("body").removeClass("loading");
					$("#to-hide").removeClass("opa");
						if(data.status != "0"){
							var str = data.data.uri;
							var splited = str.split(":");
							var replaced = str.replace(splited[1], "//"+ $('#camera_username'+count).val() + ":" + $('#camera_password'+count).val() +"@" + $('#rtsp_url'+count).val());
							$("#rtsp_url"+count).val(replaced);
							alert("Camera Url Correct!");
							$("#rtsp_url"+count).addClass(".border-green");
							$("#camera_username"+count).addClass("disabled");
							$("#camera_password"+count).addClass("disabled");
							$("#rtsp_port"+count).addClass("disabled");
							$(".btn").removeAttr("disabled");
						}else if (data.status == "0") {
							alert(data.error_msg);
							$("#rtsp_url"+count).val("");
							$("#rtsp_username"+count).val("");
							$("#rtsp_password"+count).val("");
							$("#rtsp_port"+count).val("");

						}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					$("body").removeClass("loading");
					$("#to-hide").removeClass("opa");
			        alert("Status: " + textStatus); alert("Error: " + errorThrown); 
			        $("#addCamera").attr("disabled" , "true");
			    }
			});
		}

		function editSite(el){
			var ser = $(el).parent("form").serialize();
			$.ajax({
			    url: '<?php echo base_url(); ?>Sites/editSite?attr=<?php echo $_SESSION["user_id"]; ?>',
			    type: 'post',
			    data: ser,
			    dataType: 'json',
			    success: function (data) {
			    	if(data == "success"){
			    		location.reload();
			    	}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					
			    }
			});

			console.log(ser);

		}
		function editCamera(ele){
			var ser = $(ele).parent("form").serialize();
			$.ajax({
			    url: '<?php echo base_url(); ?>Camera/editCamera?attr=<?php echo $_SESSION["user_id"]; ?>',
			    type: 'post',
			    data: ser,
			    dataType: 'json',
			    success: function (data) {
			    	if(data == "success"){
			    		location.reload();
			    	}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					
			    }
			});

			console.log(ser);
		}
		function submitCamera(elem){
			var ser = $(elem).parent("form").serialize();
			$.ajax({
			    url: '<?php echo base_url(); ?>Camera/submitCamera?attr=<?php echo $_SESSION["user_id"]; ?>',
			    type: 'post',
			    data: ser,
			    dataType: 'json',
			    success: function (data) {
			    	if(data == "success"){
			    		location.reload();
			    	}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					
			    }
			});

			console.log(ser);
		}

		function  deleteCamera(eleme){
			$.ajax({
			    url: '<?php echo base_url(); ?>Camera/deleteCamera?attr=<?php echo $_SESSION["user_id"]; ?>&id='+$(eleme).attr("camera"),
			    type: 'get',
			    success: function (data) {
			    	if(data == "success"){
			    		location.reload();
			    	}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					
			    }
			});
		}
                
</script>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Live Patrol">
    <meta name="author" content="Cornelius">
    <title>Live Patrol Admin Portal</title>

    <!-- All the files that are required -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <script src = "<?php echo base_url();?>assets/js/jquery.js"></script>  
</head>


<body <?php if($view_file != "loginform" ){ ?> id= "not-login" <?php }else{ ?> id="login-page" <?php  } ?>>
            <?php
                if($view_file != "loginform"){ ?>
                    <nav class="navbar navbar-default" style="padding-top:40px">
                        <div class="container-fluid">
                          <div class="navbar-header">
                              <a class="navbar-brand" style="margin-top:-45px" href="<?php echo base_url(); ?>Dashboard/index?attr=<?php echo $_GET['attr']; ?>"><img src= "<?php echo base_url(); ?>assets/images/logo.png" style="width: 50%"></a>
                          </div>
                          <ul class="nav navbar-nav">
                            <li class=""><a href="<?php echo base_url(); ?>Dashboard/index?attr=<?php echo $_GET['attr']; ?>">dashboard</a></li>
                            <?php 
                                if($this->session->userdata('user_role') == "admin" || $this->session->userdata('user_role') == "su"){ ?>
                                    <li><a href="<?php echo base_url(); ?>Client?attr=<?php echo $_GET['attr']; ?>">clients</a></li>
                                    <li><a href="<?php echo base_url(); ?>Sites/allSites?attr=<?php echo $_GET['attr']; ?>">sites</a></li> 
                            <?php }
                            if($this->session->userdata('user_role') == "client"){ ?>
                                <li><a href="<?php echo base_url(); ?>User/clientUsers?attr=<?php echo $_GET['attr']; ?>">client users</a></li>
                            <?php }
                                if($this->session->userdata('user_role') == "su" ){ ?>
                                    <li><a href="<?php echo base_url(); ?>User/userList?attr=<?php echo $_GET['attr']; ?>">lp users</a></li> 
                            <?php }
                            ?>
                            <li><a href="<?php echo base_url(); ?>User/logout">logout</a></li> 
                          </ul>
                        </div>
                    </nav>
                <?php }else{ ?>
                    <div id = "">
                <?php }
            
                if(!isset($view_file)){
                    die("Please set view file");
                }
                if(!isset($module)){
                    $module = $this->uri->segment(1);
                }
                if(($view_file != "") && ($module != "")){
                $path = $module."/".$view_file;
                //die("<h1>".$path."</h1>");
                $this->load->view($path);
                }
            ?>
        
        <footer class="navbar navbar-default navbar-fixed-bottom down-nav" style ="margin-top:25px; color:#000" >
            <div class="container" style="padding-top:15px;">
                <div class="col-md-6">
                    A project for Live Patrol
                 </div>

                <div class="col-md-6">
                </div>
            </div>
        </footer>
        <?php
            if($view_file != "loginform" && $view_file != "registerform" ){ ?>
        </div>
        <?php } ?>
  
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

<script>
    
</script>
        
</body>
</html>
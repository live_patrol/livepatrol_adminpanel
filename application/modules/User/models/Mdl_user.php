<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_user extends CI_Model {

function __construct() {
parent::__construct();
}

function get_table() {
    $table = "livepatrol_user";
    return $table;
}

function insert_user($data){
    $table = $this->get_table();
    $this->db->insert($table, $data);
}

function insert_client_user($data){
    $table = "user";
    $this->db->insert($table, $data);
}

function insert_user_creds($data){
    $table = 'user_creds';
    $this->db->insert($table, $data);
}
function get($order_by) {
    $table = $this->get_table();
    $this->db->order_by($order_by);
    $query=$this->db->get($table);
    return $query;
}
function get_events() {
    $table = "events";
    $this->db->order_by("id");
    $this->db->where('is_approved', false);
    $query=$this->db->get($table);
    return $query;
}

function get_where($id) {
    $table = $this->get_table();
    $this->db->where('userId', $id);
    $query=$this->db->get($table);
    return $query;
}

function get_where_user_client($id) {
    $table = "user";
    $this->db->where('id', $id);
    $query=$this->db->get($table);
    return $query;
}

function get_where_user($id) {
    $table = "livepatrol_user";
    $this->db->where('user_role', $id);
    $query=$this->db->get($table);
    return $query;
}


function _update($id, $data) {
    $table = 'livepatrol_user';
    $this->db->where('id', $id);
    $this->db->update($table, $data);
}

function _updateClientUser($id, $data) {
    $table = 'user';
    $this->db->where('id', $id);
    $this->db->update($table, $data);
}

function _update_app_user($id, $data) {
    $table = 'app_users';
    $this->db->where('app_user_id', $id);
    $this->db->update($table, $data);
}
function get_max() {
    $table = $this->get_table();
    $this->db->select_max('id');
    $query = $this->db->get($table);
    $row=$query->row();
    $id=$row->id;
    return $id;
}

function pword_check($username, $pword){
    $table = $this->get_table();
    $this->db->where('username', $username);
    $this->db->where('password', $pword);
    $this->db->where('active', 1);
    $query=$this->db->get($table);
    $num_rows = $query->num_rows();
    if($num_rows>0){
            return TRUE;
    }else {
            return FALSE;
    }
}



function check_app_user_creds($data){
    $table = 'app_users_creds';
    $this->db->where('username', $data['username']);
    $this->db->where('password', $data['password']);
    $query=$this->db->get($table);
    $num_rows = $query->num_rows();
    if($num_rows>0){
            return TRUE;
    }else {
            return FALSE;
    }
}

function get_where_custom($col, $value) {
    $table = $this->get_table();
    $this->db->where($col, $value);
    $query=$this->db->get($table);
    return $query;
}
function get_where_custom_app_user($col, $value) {
    $table = 'user';
    $this->db->where($col, $value);
    $query=$this->db->get($table);
    return $query;
}

function _insert_app_user($data){
    $table = 'app_users';
    $this->db->insert($table, $data);
}

function _insert_event($data){
    $table = 'events';
    $this->db->insert($table, $data);
}

function _insert_app_user_creds($data){
    $table = 'app_users_creds';
    $this->db->insert($table, $data);
}


function get_max_app_user_id() {
    $table = 'app_users_creds';
    $this->db->select_max('id');
    $query = $this->db->get($table);
    $row=$query->row();
    $id=$row->id;
    return $id;
}

function get_where_app_user($id) {
    $table = "app_users";
    $this->db->where('app_user_id', $id);
    $query=$this->db->get($table);
    return $query;
}

function get_where_username($username){
    $table = $this->get_table();
    $this->db->where('username', $username);
    $query=$this->db->get($table);
    $num_rows = $query->num_rows();
    if($num_rows>0){
        return true;
    }else {
        return false;
    }
}

function get_where_email($email){
    $table = "app_users";
    $this->db->where('email', $email);
    $query=$this->db->get($table);
    foreach($query->result() as $row){
        return $row->app_user_id;
    }
}

function get_where_app_username($username){
    $table = "app_users_creds";
    $this->db->where('username', $username);
    $query=$this->db->get($table);
    $num_rows = $query->num_rows();
    if($num_rows>0){
        return true;
    }else {
        return false;
    }
}

function get_where_app_email($email){
    $table = "app_users_creds";
    $this->db->where('email', $email);
    $query=$this->db->get($table);
    $num_rows = $query->num_rows();
    if($num_rows>0){
        return true;
    }else {
        return false;
    }
}

function _update_app_user_creds($id, $data) {
    $table = 'app_users_creds';
    $this->db->where('id', $id);
    $this->db->update($table, $data);
}

function insertUserSite($data){
    $table = 'user_sites';
    $this->db->insert($table, $data);
}

function deleteUserSite($id) {
$table = 'user_sites';
$this->db->where('user_id', $id);
$this->db->delete($table);
}

function _insertUser($data){
    $table = 'livepatrol_user';
    $this->db->insert($table, $data);
}

}
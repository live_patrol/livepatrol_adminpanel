<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Camera extends MX_Controller
{

function __construct() {
parent::__construct();
}
function index(){
	die('This is the perfect controller');
}

function addCamera(){
	Modules::run('Site_security/make_sure_is_admin');
	$data_camera['rtsp_url'] =  $_POST['rtsp_url'];
    $data_camera['rtsp_port'] =  $_POST['rtsp_port'];
    $data_camera['camera_username'] =  $_POST['camera_username'];
    $data_camera['camera_password'] =  $_POST['camera_password'];
    $data_camera['user_id'] = $_POST['user_id'];
    if($this->_insert($data_camera)){
    	echo "ok";
    }else{
    	echo "fail";
    }
}

function submitCamera(){
	$data_camera['rtsp_url'] =  $_POST['rtsp_url'];
    $data_camera['rtsp_port'] =  $_POST['rtsp_port'];
    $data_camera['camera_username'] =  $_POST['camera_username'];
    $data_camera['camera_password'] =  $_POST['camera_password'];
    $data_camera['site_id'] = $_POST['siteId'];
    $this->_insert($data_camera);
	echo json_encode("success");
}

function editCamera(){
	$data_camera['rtsp_url'] =  $_POST['rtsp_url'];
    $data_camera['rtsp_port'] =  $_POST['rtsp_port'];
    $data_camera['camera_username'] =  $_POST['camera_username'];
    $data_camera['camera_password'] =  $_POST['camera_password'];
    $this->_update( $_POST['cameraId'], $data_camera);
    echo json_encode("success");
}

function deleteCamera(){
	$this->_delete($_GET['id']);
	echo json_encode("success");
}

function deleteSiteCameras($siteId){
	$this->load->model('Mdl_camera');
	$this->Mdl_camera->deleteSiteCameras($siteId);
}

function get_camera_where_user($userId){
	$this->load->model('Mdl_camera');
	return $this->Mdl_camera->get_camera_where_user($userId);
}

function get_camera_where_site($siteId){
	$this->load->model('Mdl_camera');
	return $this->Mdl_camera->get_camera_where_site($siteId);
}

function get($order_by) {
$this->load->model('Mdl_camera');
$query = $this->Mdl_camera->get($order_by);
return $query;
}

function get_with_limit($limit, $offset, $order_by) {
$this->load->model('Mdl_camera');
$query = $this->Mdl_camera->get_with_limit($limit, $offset, $order_by);
return $query;
}

function get_where($id) {
$this->load->model('Mdl_camera');
$query = $this->Mdl_camera->get_where($id);
return $query;
}

function get_where_custom($col, $value) {
$this->load->model('Mdl_camera');
$query = $this->Mdl_camera->get_where_custom($col, $value);
return $query;
}

function _insert($data) {
$this->load->model('Mdl_camera');
$this->Mdl_camera->_insert($data);
}

function _update($id, $data) {
$this->load->model('Mdl_camera');
$this->Mdl_camera->_update($id, $data);
}

function _delete($id) {
	
$this->load->model('Mdl_camera');
$this->Mdl_camera->_delete($id);
}

function count_where($column, $value) {
$this->load->model('Mdl_camera');
$count = $this->Mdl_camera->count_where($column, $value);
return $count;
}

function get_max() {
$this->load->model('Mdl_camera');
$max_id = $this->Mdl_camera->get_max();
return $max_id;
}

function _custom_query($mysql_query) {
$this->load->model('Mdl_camera');
$query = $this->Mdl_camera->_custom_query($mysql_query);
return $query;
}

}
<div class="container">
	  <h2>All Users</h2>
	  <div class="right-inner-addon ">
		    <i class="icon-search"></i>
		    <input type="search" class="form-control" placeholder="Search" />
		</div>      <br><br>
	  <table class="table" style="margin-bottom: 50px">
	    <thead>
	      <tr>
	        <th>name</th>
	        <th>status</th>
	        <th>actions</th>
	      </tr>
	    </thead>
	    <tbody>
              <?php foreach($query->result() as $row){ ?>
                <tr>
                    <td><?php echo $row->firstname . $row->lastname;  ?></td>
                    <td><?php echo $row->active;  ?></td>
                    <td><a href ="<?php echo base_url(); ?>User/editClientUser?attr=<?php echo $_SESSION['user_id']; ?>&id=<?php echo $row->id; ?>">Add Sites</a><a  <?php if($row->active == "1"){?> style="color: green!important" <?php }else{?> style="color: black!important" <?php } ?> href = "<?php echo base_url(); ?>User/activateClientUser?id=<?php echo $row->id; ?>&userId=<?php echo  $row->id; ?>&attr=<?php echo $_GET['attr']; ?>">active</a> <a <?php if($row->active == "0"){?> style="color: red!important" <?php }else{ ?> style="color: black!important" <?php } ?> href = "<?php echo base_url(); ?>User/deactivateClientUser?id=<?php echo $row->id; ?>&userId=<?php echo  $row->id; ?>&attr=<?php echo $_GET['attr']; ?>">de-active</a></td>
                </tr>
              <?php } ?>
	    </tbody>
	  </table>
	  <a href="<?php echo base_url(); ?>User/addClientUser?attr=<?php echo $_SESSION['user_id']; ?>"><span class="btn btn-default" style="width:100%; margin-bottom:200px" >Add User</span></a>
	</div>
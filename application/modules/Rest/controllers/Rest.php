<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Rest extends MX_Controller
{

function __construct() {
parent::__construct();
}
function index(){
	die('This is the perfect controller');
}

function checkCreds(){
	if(empty($_POST['username'])){
		$error['code'] = '400';
		$error['message'] = 'Username Empty';
		$sapi_type = php_sapi_name();
		if (substr($sapi_type, 0, 3) == 'cgi')
		    header("Status: 401 Username Empty");
		else
		    header("HTTP/1.1 401 Username Empty");
		header('Content-Type: application/json');
		echo json_encode($error); 
		exit();
	}
	if(empty($_POST['password'])){
		$error['code'] = '400';
		$error['message'] = 'Password Empty';

		$sapi_type = php_sapi_name();
		if (substr($sapi_type, 0, 3) == 'cgi')
		    header("Status: 401 Password Empty");
		else
		    header("HTTP/1.1 401 Password Empty");
		header('Content-Type: application/json');
		
		echo json_encode($error); 
		exit();
	}

	$query = $this->pword_check($_POST['username'], $_POST['password']);
	if(empty($query)){
		$sapi_type = php_sapi_name();
		if (substr($sapi_type, 0, 3) == 'cgi')
		    header("Status: 401 Invalid Credentials");
		else
		    header("HTTP/1.1 401 Invalid Credentials");
		header('Content-Type: application/json');

		$error['code'] = '401';
		$error['message'] = 'Invalid Credentials';
		echo json_encode($error);
	}else{
		$accessToken = $query['accessToken'];
		if(empty($accessToken)){
			$accessToken = md5($query['username'] . $query['lastname']);
			$dataUser['access_token'] = $accessToken;
			if($this->addAccessToken($dataUser, $query['id'])){
				$sapi_type = php_sapi_name();
				if (substr($sapi_type, 0, 3) == 'cgi')
				    header("Status: 401 Invalid Credentials");
				else
				    header("HTTP/1.1 401 Invalid Credentials");
				header('Content-Type: application/json');
				echo json_encode($query);
			}else{
				echo 'Error';
			}
		}else{
			$sapi_type = php_sapi_name();
			if (substr($sapi_type, 0, 3) == 'cgi')
			    header("Status: 200 OK");
			else
			    header("HTTP/1.1 200 OK");
			header('Content-Type: application/json');
			echo json_encode($query);
		}
	}
}


function addAccessToken($accessToken, $id){
	$this->load->model('Mdl_rest');
	return $this->Mdl_rest->insertAccessToken($accessToken, $id);
}

function pword_check($username, $pword){
	$pword_hash = md5($pword);
	$this->load->model('Mdl_rest');
	$result = $this->Mdl_rest->pword_check($username, $pword_hash);
	return $result;
}

function getCameras(){
	if(!isset($_POST['access_token'])){
		$sapi_type = php_sapi_name();
			if (substr($sapi_type, 0, 3) == 'cgi')
			    header("Status: 401 Empty Access Token");
			else
			    header("Status: 401 Empty Access Token");
		header('Content-Type: application/json');
		$error['code'] = '401';
		$error['message'] = 'Sorry, no access token';
		echo json_encode($error);
		exit();
	}else{
		$accessToken = $_POST['access_token'];
	}
	if(!isset($_POST['user_id'])){
		$sapi_type = php_sapi_name();
			if (substr($sapi_type, 0, 3) == 'cgi')
			    header("Status: 401 Empty Access Token");
			else
			    header("Status: 401 Empty Access Token");
		header('Content-Type: application/json');
		$error['code'] = '401';
		$error['message'] = 'Sorry, no user id';
		echo json_encode($error);
		exit();
	}else{
		$userId = $_POST['user_id'];
	}
	if($this->checkAccesstoken($userId, $accessToken)){
		$query=$this->fetchCameras($userId);
		if($query->num_rows() > 0){
			$sapi_type = php_sapi_name();
			if (substr($sapi_type, 0, 3) == 'cgi')
			    header("Status: 200 OK");
			else
			    header("Status: 200 OK");
			$data['camerasList'] = $query->result();
			header('Content-Type: application/json');
			echo json_encode($data);
		}else{
			$sapi_type = php_sapi_name();
			if (substr($sapi_type, 0, 3) == 'cgi')
			    header("Status: 401 No Cameras");
			else
			    header("Status: 401 No Cameras");
			header('Content-Type: application/json');
			$error['code'] = '401';
			$error['message'] = 'Sorry there are no cameras set for you yet!';
			echo json_encode($error);
			exit();
		}
	}else{
		$sapi_type = php_sapi_name();
			if (substr($sapi_type, 0, 3) == 'cgi')
			    header("Status: 401 Invalid Access Token");
			else
			    header("Status: 401 Invalid Access Token");
		header('Content-Type: application/json');
		$error['code'] = '401';
		$error['message'] = 'Access Token Not Valid';
		echo json_encode($error);
		exit();
	}
}

function checkAccesstoken($userId, $accessToken){
	$this->load->model('Mdl_rest');
	$result = $this->Mdl_rest->checkAccesstoken($userId, $accessToken);
	return $result;
}

function fetchCameras($userId){
	$this->load->model('Mdl_rest');
	return $this->Mdl_rest->fetchCameras($userId);	
}

function get($order_by) {
$this->load->model('Mdl_rest');
$query = $this->Mdl_rest->get($order_by);
return $query;
}

function get_with_limit($limit, $offset, $order_by) {
$this->load->model('Mdl_rest');
$query = $this->Mdl_rest->get_with_limit($limit, $offset, $order_by);
return $query;
}

function get_where($id) {
$this->load->model('Mdl_rest');
$query = $this->Mdl_rest->get_where($id);
return $query;
}

function get_where_custom($col, $value) {
$this->load->model('Mdl_rest');
$query = $this->Mdl_rest->get_where_custom($col, $value);
return $query;
}

function _insert($data) {
$this->load->model('Mdl_rest');
$this->Mdl_rest->_insert($data);
}

function _update($id, $data) {
$this->load->model('Mdl_rest');
$this->Mdl_rest->_update($id, $data);
}

function _delete($id) {
$this->load->model('Mdl_rest');
$this->Mdl_rest->_delete($id);
}

function count_where($column, $value) {
$this->load->model('Mdl_rest');
$count = $this->Mdl_rest->count_where($column, $value);
return $count;
}

function get_max() {
$this->load->model('Mdl_rest');
$max_id = $this->Mdl_rest->get_max();
return $max_id;
}

function _custom_query($mysql_query) {
$this->load->model('Mdl_rest');
$query = $this->Mdl_rest->_custom_query($mysql_query);
return $query;
}

}
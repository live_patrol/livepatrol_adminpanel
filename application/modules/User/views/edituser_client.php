<style>
	.border-green{
		border: 6px solid green;
	}
	.border-red{
		border: 6px solid red;
	}
	.loading{
		background-image: url("../assets/images/default.gif");
		background-repeat: no-repeat;
	    background-attachment: fixed;
	    background-position: center; 
	}
	.opa{
		opacity : 0;
	}
</style>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-select.js"></script>
<div class="text-center" style="padding:50px 0" id="to-hide" >
	<div class="logo">edit site</div>
	<!-- Main Form -->
        	<?php 
        		foreach ($user->result() as $row) {  ?>
        		<div class="main-login-form">
					<div class="container-fluid">
						<form id ="site-form" method="post" action="<?php echo base_url(); ?>User/editClientUserSubmit?attr=<?php echo $_SESSION['user_id']; ?>">
							<input type="hidden" id="userId" name="userId" value="<?php echo $row->id; ?>" />
							<div class="form-group col-lg-6">
		                        <input type="text" class="form-control" id="firstname" name="firstname" placeholder="First Name" value="<?php echo $row->firstname; ?>" required />
							</div>
							<div class="form-group col-lg-6">
		                        <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last Name" value="<?php echo $row->lastname; ?>" required />
							</div>
							<div class="form-group col-lg-6">
		                        <input type="text" class="form-control" id="password" name="password" placeholder="Password" value="<?php echo $row->password; ?>" required />
							</div>
							<div class="form-group col-lg-6">
		                        <input type="text" class="form-control" id="phone" name="phone" placeholder="phone" value="<?php echo $row->phone; ?>" required />
							</div>
							<div class="col-lg-12">
								<label class="text-center" style="color:black">Select Sites</label>
							</div>
							<div class="col-lg-12">
									<select class="selectpicker" name="sites[]" id="company" data-live-search="true" style="width:100%" multiple >
									<?php
									foreach ($sites->result() as $row1) { ?>
										<option value="<?php echo $row1->id; ?>"><?php echo $row1->site_name; ?></option>
									<?php }
									?>
		                            </select>
							</div>
							<input type="submit" class="btn btn-default" style="width:50%; margin-top:30px" value="Edit site" />
						</form>
					</div><br>
				</div>
        	<?php }
        	?>
	<!-- end:Main Form -->
</div>


<script>
		$(document).ready(function(){
			$('.selectpicker').selectpicker();
			$("#company").addClass("hidden");
			$(".glyphicon").addClass("hidden");
		});
                
</script>



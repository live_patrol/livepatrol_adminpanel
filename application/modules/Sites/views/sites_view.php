<div class="container">
	  <h2>All Sites</h2>
	  <div class="right-inner-addon ">
		    <i class="icon-search"></i>
		    <input type="search" class="form-control" placeholder="Search" />
		</div><br><br>
	  <table class="table">
	    <thead>
	      <tr>
	        <th>ID</th>
	        <th>Name</th>
	        <th>actions</th>
	      </tr>
	    </thead>
	    <tbody>
              <?php foreach($sites->result() as $row){ ?>
                <tr>
                    <td><?php echo $row->id;  ?></td>
                    <td><?php echo  $row->site_name; ?></td>
                    <td><a style="color:black!important" href = "<?php echo base_url(); ?>Sites/edit?id=<?php echo $row->id; ?>&attr=<?php echo $_GET['attr']; ?>">edit</a> 
                    	<?php if($_SESSION['user_role'] == 'su'){ ?>
                    		<a style="color:red!important" href = "<?php echo base_url(); ?>Sites/delete?id=<?php echo $row->id; ?>&attr=<?php echo $_GET['attr']; ?>" onclick="confirmDelete(this, event)" >delete</a>
                    	<?php }?>
                	</td>
                </tr>
              
              <?php } ?>
	      
	    </tbody>
	  </table>
                <div class ="container" style="margin-bottom: 500px">
                    <span><a class ="btn btn-default" href = "<?php echo base_url(); ?>Sites/add?attr=<?php echo $_GET['attr']; ?>" style = "width:100%">Add Site</a></span>
                </div>      
	</div>
	<script>
		function confirmDelete(el, ev){
			var confirm = window.confirm("Are you sure you want to delete this?");
			if(confirm){
				window.location.href = $(el).attr("href");
			}else{
				ev.preventDefault();
			}
		}
	</script>
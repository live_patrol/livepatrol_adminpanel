<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Site_security extends MX_Controller
{

function __construct() {
parent::__construct();
}

function make_hash($pword){
	$safe_pass = md5($password);
	echo $safe_pass;
}
function make_sure_is_admin(){
	$username = $this->session->userdata('username');
        
	$user_id = $this->session->userdata('user_id');
	
	//die($_GET['attr']);
	if(!isset($username) || !isset($user_id) || $_GET['attr'] != $user_id){
		$this->session->unset_userdata('username', $username);
		$this->session->unset_userdata('user_id', $user_id);
		redirect(base_url());
	}
}

function make_sure_is_not_client(){
	$username = $this->session->userdata('username');
        
	$user_id = $this->session->userdata('user_id');

	$user_role = $this->session->userdata('user_role');
	
	//die($_GET['attr']);
	if(!isset($username) || !isset($user_id) || $_GET['attr'] != $user_id || $user_role == "client"){
		$this->session->unset_userdata('username', $username);
		$this->session->unset_userdata('user_id', $user_id);
		redirect(base_url());
	}
}

function check_preference(){
	//die($_GET['attr']);
	$username = $_GET['attr'];
	
	$result = false;
	$this->load->module('Users');
	$query = $this->users->get_where1($username);
	foreach($query->result() as $row){
		$user_pref_array = explode(',', $row->user_preference);		
		
		if($this->uri->rsegment(1) == "Bookings"){
			//die('Bookings');
			for($i=0; $i<count($user_pref_array); $i++){
				if($user_pref_array[$i] == 'booking'){
					$result = true;
				}				
			}
			
		}
		
		if($this->uri->rsegment(1) == "Invoices"){
			//die('Ivoices');
			for($i=0; $i<count($user_pref_array); $i++){
				if($user_pref_array[$i] == 'invoice'){
					$result = true;
				}				
			}	
		}
		if($result == false){
			die('Sorry you are not yet subscribed to this offer. Upgrade to premium account to enjoy full access. :)');
		}	
	}
}
}
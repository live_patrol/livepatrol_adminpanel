<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard extends MX_Controller
{
function __construct() {
parent::__construct();
Modules::run('Site_security/make_sure_is_admin');
}


function index(){
    $this->load->module("Main_template");
    $id = $this->session->userdata('username');
    $data['query'] = $this->get_where($id);
    $data['view_file'] = 'dashboard';
    $data['module'] = 'Dashboard';
    $this->main_template->index($data);
}



function editPro(){
    $data['firstname'] = $_POST['firstname'];
    $data['lastname'] = $_POST['lastname'];
    $data['email'] = $_POST['email'];
    $data['phone'] = $_POST['phone'];
    $this->_update($_POST['id'], $data);
    $data_creds['username'] = $_POST['email'];
    $this->_update_creds($_POST['id'], $data_creds);
    
}

function get($order_by) {
    $this->load->model('mdl_dashboard');
    $query = $this->mdl_dashboard->get($order_by);
    return $query;
}
function get_where($id) {
    $this->load->model('mdl_dashboard');
    $query = $this->mdl_dashboard->get_where($id);
    return $query;
}

function get_where_custom($col, $value) {
    $this->load->model('mdl_dashboard');
    $query = $this->mdl_dashboard->get_where_custom($col, $value);
    return $query;
}


function get_max() {
    $this->load->model('mdl_dashboard');
    $max_id = $this->mdl_dashboard->get_max();
    return $max_id;
}

function _update($id, $data) {
    $this->load->model('mdl_dashboard');
    $this->mdl_dashboard->_update($id, $data);
}
function _update_creds($id, $data) {
    $this->load->model('mdl_dashboard');
    $this->mdl_dashboard->_update_creds($id, $data);
}
}
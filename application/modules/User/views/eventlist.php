<div class="container">
	  <h2>All Users</h2>
	  <div class="right-inner-addon ">
		    <i class="icon-search"></i>
		    <input type="search" class="form-control" placeholder="Search" />
		</div>      <br><br>
	  <table class="table">
	    <thead>
	      <tr>
	        <th>event name</th>
	        <th>event type</th>
                <th>event speaker</th>
                <th>event organiser</th>
                <th>event description</th>
	        <th>requestee</th>
                <th>action</th>
                
	      </tr>
	    </thead>
	    <tbody>
              <?php foreach($query->result() as $row){ ?>
                <tr>
                    <td><?php echo $row->eventname;  ?></td>
                    <td><?php echo $row->eventtype;  ?></td>
                    <td><?php echo $row->speaker;  ?></td>
                    <td><?php echo $row->organiser;  ?></td>
                    
                    <td><?php echo $row->eventdetails;  ?></td>
                    
                    <td><a href = "<?php echo base_url(); ?>User/appUserView?attr=<?php echo $_GET['attr']; ?>&app_id=<?php echo $row->app_user_id; ?>">User Info</a></td>
                    <td><a href = "<?php echo base_url(); ?>User/updateEvent?attr=<?php echo $_GET['attr']; ?>&event_id=<?php echo $row->id; ?>">Approve</a></td>
                    
<!--                    <td><a href = "<?php echo base_url(); ?>User/activate?id=<?php echo $row->id; ?>&userId=<?php echo  $row->id; ?>&attr=<?php echo $_GET['attr']; ?>">activate</a> <a href = "<?php echo base_url(); ?>User/deactivate?id=<?php echo $row->id; ?>&userId=<?php echo  $row->id; ?>&attr=<?php echo $_GET['attr']; ?>">de-activate</a></td>-->
                </tr>
              
              <?php } ?>
	      
	    </tbody>
	  </table>
	</div>
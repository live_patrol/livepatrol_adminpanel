<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Sites extends MX_Controller
{

function __construct() {
parent::__construct();
}
function index(){
	die('This is the perfect controller');
}
function allSites(){
	Modules::run('Site_security/make_sure_is_admin');
	Modules::run('Site_security/make_sure_is_not_client');
	$data['sites'] = $this->get("id");
	$this->load->module("Main_template");
    $data['view_file'] = 'sites_view';
    $data['module'] = 'Sites';
    $this->main_template->index($data);

}

function edit(){
	Modules::run('Site_security/make_sure_is_admin');
	Modules::run('Site_security/make_sure_is_not_client');
	$data['site'] = $this->get_where($_GET['id']);
	$this->load->module("Camera");
	$data["cameras"] = $this->camera->get_camera_where_site($_GET['id']);
	$this->load->module("Client");
	$data['clients'] = $this->client->get("id");
	$this->load->module("Main_template");
    $data['view_file'] = 'editsite_view';
    $data['module'] = 'Sites';
    $this->main_template->index($data);

}

function editSite(){
	// echo json_encode($_POST['sitename']);
	$data_site['site_name'] = $_POST['sitename'];
	$data_site['company'] = $_POST['company'];
	$this->_update($_POST['siteId'], $data_site);
	echo json_encode("success");
}

function get($order_by) {
Modules::run('Site_security/make_sure_is_admin');
Modules::run('Site_security/make_sure_is_not_client');
$this->load->model('Mdl_sites');
$query = $this->Mdl_sites->get($order_by);
return $query;
}

function add(){
	Modules::run('Site_security/make_sure_is_not_client');
	Modules::run('Site_security/make_sure_is_admin');
	$this->load->module("Main_template");
	$this->load->module("Client");
	$data['clients'] = $this->client->get("id");
    $data['view_file'] = 'addsite_view';
    $data['module'] = 'Sites';
    $this->main_template->index($data);
}

function addSite(){
	Modules::run('Site_security/make_sure_is_admin');
	// Modules::run('Site_security/make_sure_is_not_client');
	// echo $_POST['clientId'];
	$site=$_POST['siteCounter'];
	$data['user_id'] = $_POST['clientId'];
	$data['site_name'] = $_POST['site_name'.$site];
	$this->_insert($data);
	$this->load->module("Camera");
	if(isset($_POST['cameraCounter'.$site])){
		for ($x = 1; $x <= $_POST['cameraCounter'.$site]; $x++) {
		    $data_camera["user_id"] = $_POST['clientId'];
		  	$data_camera["site_id"] = $this->get_max();
		    $data_camera["rtsp_url"] = $_POST['rtsp_url'.$site.$x];
		    $data_camera["rtsp_port"] = $_POST['rtsp_port'.$site.$x];
		    $data_camera["camera_username"] = $_POST['camera_username'.$site.$x];
		    $data_camera["camera_password"] = $_POST['camera_password'.$site.$x];
		    $this->camera->_insert($data_camera);
		} 
	}
	echo "okay";

}

function submitSite(){
	Modules::run('Site_security/make_sure_is_admin');
	Modules::run('Site_security/make_sure_is_not_client');
	$data_site['site_name'] = $_POST["name"];
	$data_site['company'] = $_POST["company"];
	$this->_insert($data_site);
	if(isset($_POST['cameraCounter'])){
	    $cameraCounter = $_POST['cameraCounter'];
	    for ($i = 1; $i <= $cameraCounter ; $i++) {
	            $data_camera['rtsp_url'] =  $_POST['rtsp_url'.$i];
	            $data_camera['rtsp_port'] =  $_POST['rtsp_port'.$i];
	            $data_camera['camera_username'] =  $_POST['camera_username'.$i];
	            $data_camera['camera_password'] =  $_POST['camera_password'.$i];
	            $data_camera['site_id'] = $this->get_max();
	            $this->load->module('Camera');
	            $this->camera->_insert($data_camera);
	    }
	}
	$this->allSites();
}

function deleteSite(){
	Modules::run('Site_security/make_sure_is_admin');
	Modules::run('Site_security/make_sure_is_not_client');
	$this->_deleteSite($_GET['id']);
	$this->load->module('Camera');
	$this->camera->deleteSiteCameras($_GET['id']);
	echo "okay";
}

function delete(){
	Modules::run('Site_security/make_sure_is_admin');
	Modules::run('Site_security/make_sure_is_not_client');
	$this->_deleteSite($_GET['id']);
	$this->load->module('Camera');
	$this->camera->deleteSiteCameras($_GET['id']);
	$this->allSites();
}

function get_with_limit($limit, $offset, $order_by) {
$this->load->model('Mdl_sites');
$query = $this->Mdl_sites->get_with_limit($limit, $offset, $order_by);
return $query;
}

function get_where($id) {
$this->load->model('Mdl_sites');
$query = $this->Mdl_sites->get_where($id);
return $query;
}

function get_where_custom($col, $value) {
$this->load->model('Mdl_sites');
$query = $this->Mdl_sites->get_where_custom($col, $value);
return $query;
}

function get_site_where_user($userId){
	$this->load->model('Mdl_sites');
	return $this->Mdl_sites->get_site_where_user($userId);
}

function _insert($data) {
$this->load->model('Mdl_sites');
$this->Mdl_sites->_insert($data);
}

function _update($id, $data) {
$this->load->model('Mdl_sites');
$this->Mdl_sites->_update($id, $data);
}

function _delete($id) {
$this->load->model('Mdl_sites');
$this->Mdl_sites->_delete($id);
}

function _deleteSite($id){
$this->load->model('Mdl_sites');
$this->Mdl_sites->_deleteSite($id);
}

function count_where($column, $value) {
$this->load->model('Mdl_sites');
$count = $this->Mdl_sites->count_where($column, $value);
return $count;
}

function get_max() {
$this->load->model('Mdl_sites');
$max_id = $this->Mdl_sites->get_max();
return $max_id;
}

function _custom_query($mysql_query) {
$this->load->model('Mdl_sites');
$query = $this->Mdl_sites->_custom_query($mysql_query);
return $query;
}

}
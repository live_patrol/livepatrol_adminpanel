<div class="container">
	  <h2>All Users</h2>
	  <div class="right-inner-addon ">
		    <i class="icon-search"></i>
		    <input type="search" class="form-control" placeholder="Search" />
		</div>      <br><br>
	  <table class="table" style="margin-bottom: 50px">
	    <thead>
	      <tr>
	        <th>username</th>
	        <th>status</th>
	        <th>actions</th>
	      </tr>
	    </thead>
	    <tbody>
              <?php foreach($query->result() as $row){ ?>
                <tr>
                    <td><?php echo $row->username;  ?></td>
                    <td><?php echo $row->active;  ?></td>
                    <td><a  <?php if($row->active == "1"){?> style="color: green!important" <?php }else{?> style="color: black!important" <?php } ?> href = "<?php echo base_url(); ?>User/activate?id=<?php echo $row->id; ?>&userId=<?php echo  $row->id; ?>&attr=<?php echo $_GET['attr']; ?>">active</a> <a <?php if($row->active == "0"){?> style="color: red!important" <?php }else{ ?> style="color: black!important" <?php } ?> href = "<?php echo base_url(); ?>User/deactivate?id=<?php echo $row->id; ?>&userId=<?php echo  $row->id; ?>&attr=<?php echo $_GET['attr']; ?>">de-active</a></td>
                </tr>
              <?php } ?>
	    </tbody>
	  </table>

  	<div class ="container" style="margin-bottom: 500px">
        <span><a class ="btn btn-default" href = "<?php echo base_url(); ?>User/registerform?attr=<?php echo $_GET['attr']; ?>" style = "width:100%">Add User</a></span>
    </div>
	</div>
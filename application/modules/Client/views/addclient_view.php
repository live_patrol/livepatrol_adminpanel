<style>
	.border-green{
		border: 6px solid green;
	}
	.border-red{
		border: 6px solid red;
	}
	.loading{
		background-image: url("../assets/images/default.gif");
		background-repeat: no-repeat;
	    background-attachment: fixed;
	    background-position: center; 
	}
	.opa{
		opacity : 0;
	}
</style>
<div class="text-center" style="padding:50px 0" id="to-hide" >
	<div class="logo">add client</div>
	<!-- Main Form -->
	<div class="">
            <form id="add-client-form" enctype="multipart/form-data" action ="<?php echo base_url(); ?>Client/submitClient?attr=<?php echo $_GET['attr']; ?>" method ="post" class="text-left">
			<div class="main-login-form">
				<div class="container-fluid">
					<div class="form-group col-lg-6">
                        <input type="text" class="form-control" id="companyname" name="companyname" placeholder="Company Name" required />
					</div>
					<div class="form-group col-lg-6">
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone" required />
					</div>
					<div class="form-group col-lg-6">
                        <input type="email" class="form-control" id="username" name="username" placeholder="username/email" required />
					</div>
					<div class="form-group col-lg-6">
                        <input type="text" class="form-control" id="password" name="password" placeholder="password" required />
					</div>
					<!-- <div class="form-group col-lg-6">
                        <input type="text" class="form-control" id="username" name="username" placeholder="username" required />
                        <span style ="color:red" id ="user_check" ></span>
					</div>
					<div class="form-group col-lg-6">
                        <input type="text" class="form-control" id="password" name="password" placeholder="password" required />
					</div>
					<div class="form-group col-lg-12">
                        <textarea class="form-control" id="client_details" name="client_details" placeholder="client description"></textarea>
					</div>
					<div id = "siteDiv" class = "container-fluid">
						
					</div>
					<span id = "addSite" class="btn btn-default" style = "width:100%" >add site</span> -->
				</div><br><br>
				<div class="container">
					<span id ="submitClient" type ="submit" class="btn btn-default" style = "width:80%!important">add client</span>
				</div>
			</div>
			
		</form>
	</div>
	<!-- end:Main Form -->
</div>


<script>
		$(document).ready(function(){


			$("#username").focusout(function(){
	            var obj = {
	                username : $("#username").val()
	            }
	            $.ajax({
	                type: 'POST',
	                url: '<?php echo base_url(); ?>Client/checkUsername',
	                data: obj,
	                success: function(response) {
	                   if(response == "true"){
	                       $("#username").val("");
	                       $("#user_check").html("Username already exists. Please choose a different one.");
	                   }else{
	                       
	                   }
	                   
	                },
	                error: function(){
	                    alert("internal error");
	                }
	            });
	        });
                    
            function isEveryInputEmpty() {
                    var allEmpty = true;
                    $(':input').each(function() {
                        if ($(this).val() !== '') {
                            allEmpty = false;
                            return false; // we've found a non-empty one, so stop iterating
                        }
                    });

                    return allEmpty;
                }
			var cameraCounter = 0;
			var siteCounter = 0;

			$(document).on("click" , ".btn" , function(){
                if((isEveryInputEmpty())){
                    alert("Please fill in all feilds");
                }else{
                	var site = $(this).attr("site");
                    if($(this).attr("id") == "addCamera"+site){
                    	cameraCounter = $(this).attr("camera");
						cameraCounter++;
						var col = '<h3>camera '+cameraCounter+'</h3><span class="fa fa-2x pull-right fa-minus"></span><div class="form-group col-lg-12"><input type="hidden" id = "cameraCounter'+site+'" name="cameraCounter'+site+'" value="'+cameraCounter+'"><div class="form-group col-lg-8"><label>Camera Public URL</label><input type="text" class="form-control" id="rtsp_url'+site+cameraCounter+'" name="rtsp_url'+site+cameraCounter+'" placeholder="camera rtsp url" required /></div><div class="form-group col-lg-4"><label>Camera HTTP Port</label><input type="text" class="form-control" id="rtsp_port'+site+cameraCounter+'" name="rtsp_port'+site+cameraCounter+'" placeholder="camera http port" required /></div><div class="form-group col-lg-6"><label>Camera Username</label><input type="text" class="form-control" id="camera_username'+site+cameraCounter+'" name="camera_username'+site+cameraCounter+'" placeholder="camera Username" required /></div><div class="form-group col-lg-6"><label>Camera RTSP Password</label><input type="text" class="form-control" id="camera_password'+site+cameraCounter+'" name="camera_password'+site+cameraCounter+'" placeholder="camera password" required /></div><div class = "col-lg-12"><span class = "btn btn-default" onclick = "testCamera(this, '+site+cameraCounter+')" >Test Camera URL</span></div></div><br><br>';
						$("#cameraDiv"+site).append(col);
						$("#addCamera"+site).attr("disabled" , "true");
						$(this).attr("camera", cameraCounter);
					}else if($(this).attr("id") == "addSite"){
						cameraCounter = 0;
						siteCounter++;
						var col = '<h3>Site '+siteCounter+'</h3><input type="hidden" name="siteCounter" value="'+siteCounter+'"><span class="fa fa-2x pull-right fa-minus"></span><div class="container-fluid"><div class="form-group col-lg-12"><div class="form-group col-lg-12"><label>Sitename</label><input type="text" class="form-control" id="site_name'+siteCounter+'" name="site_name'+siteCounter+'" placeholder="Sitename" required /></div></div><div id = "cameraDiv'+siteCounter+'" class = "container-fluid"></div><span site = "'+siteCounter+'" camera = "0" id = "addCamera'+siteCounter+'" class="btn btn-default" style = "width:100%" >add camera</span><br></div><br><br>';
						$("#siteDiv").append(col);

						// $("#addSite").attr("disabled" , "true");
					}
				}
			});
			$(document).on("click" , ".fa" , function(){
				if($(this).attr("class") === "fa fa-2x pull-right fa-minus"){
					$(this).removeClass("fa-minus");
					$(this).addClass("fa-plus");
					$(this).next("div").addClass("hidden");
				}else if ($(this).attr("class") === "fa fa-2x pull-right fa-plus"){
					$(this).removeClass("fa-plus");
					$(this).addClass("fa-minus");
					$(this).next("div").removeClass("hidden");
				}
			});
            $("#submitClient").click(function(){
                $("textarea").each(function(){
                    this.value = this.value.replace(/\r?\n/g, '<br />');
                });
                $("#add-client-form").submit();
            });
            $(document).on("change" , ".lang-select" , function(){
                if($(this).val() == 1){
                    $(this).parents("div").find("textarea").addClass("text-right");
                }else{
                    $(this).parents("div").find("textarea").removeClass("text-right");
                }
            });
		});

		function testCamera(el, count){
			// alert("URL : " + $("#rtsp_url"+count).val());
			// alert("Username : " + $("#camera_username"+count).val());
			// alert("Password : " + $("#camera_password"+count).val());
			// alert("Port : " + $("#rtsp_port"+count).val());
			
			// alert($("#rtsp_port"+count).val());
			$("body").addClass("loading");
			$("#to-hide").addClass("opa");			

			// $("#addCamera").attr("disabled" , "true");
			$.ajax({
			    url: 'http://54.190.254.95:8080/checkCameraUrl',
			    type: 'post',
			    data: JSON.stringify({"cameraHost":$("#rtsp_url"+count).val(), "cameraUsername":$("#camera_username"+count).val(), "cameraPassword":$("#camera_password"+count).val(), "cameraPort":$("#rtsp_port"+count).val()}),
				contentType: "application/json;charset=utf-8",
			    dataType: 'json',
			    success: function (data) {
			    	$("body").removeClass("loading");
					$("#to-hide").removeClass("opa");
						if(data.status != "0"){
							var str = data.data.uri;
							var splited = str.split(":");
							var replaced = str.replace(splited[1], "//"+ $('#camera_username'+count).val() + ":" + $('#camera_password'+count).val() +"@" + $('#rtsp_url'+count).val());
							$("#rtsp_url"+count).val(replaced);
							alert("Camera Url Correct!");
							$("#rtsp_url"+count).addClass(".border-green");
							$("#camera_username"+count).addClass("disabled");
							$("#camera_password"+count).addClass("disabled");
							$("#rtsp_port"+count).addClass("disabled");
							$(".btn").removeAttr("disabled");
						}else if (data.status == "0") {
							alert(data.error_msg);
							$("#rtsp_url"+count).val("");
							$("#rtsp_username"+count).val("");
							$("#rtsp_password"+count).val("");
							$("#rtsp_port"+count).val("");

						}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) { 
					$("body").removeClass("loading");
					$("#to-hide").removeClass("opa");
			        alert("Status: " + textStatus); alert("Error: " + errorThrown); 
			        $("#addCamera").attr("disabled" , "true");
			    }
			});
		}
                
</script>



<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class User extends MX_Controller
{

function __construct() {
parent::__construct();
}


function index(){
    $this->load->module("Main_template");
    $data['view_file'] = 'loginform';
    $data['module'] = 'User';
    $this->main_template->index($data);
}
function registerform(){
    Modules::run('Site_security/make_sure_is_admin');
    $this->load->module("Main_template");
    $data['view_file'] = 'registerform';
    $data['module'] = 'User';
    $this->main_template->index($data);
}

function insert_user($data){
    $this->load->model("mdl_user");
    $this->mdl_user->insert_user($data);
}

function insert_client_user($data){
    $this->load->model("mdl_user");
    $this->mdl_user->insert_client_user($data);
}

function insert_user_creds($data_user_creds){
    $this->load->model("mdl_user");
    $this->mdl_user->insert_user_creds($data_user_creds);
}

function login(){
    $data['credentials'] = $this->get_where(5);
    $data['view_file'] = "loginform";
    $this->load->module('Main_template');
    $this->main_template->index($data);	
}

function submit(){
    $this->load->library('form_validation');
    $this->form_validation->set_rules('username', 'Username', 'required|max_length[30]');    
    $this->form_validation->set_rules('pword', 'Password', 'callback_pword_check');
        
    if ($this->form_validation->run($this) == FALSE)
    {
        $this->index();
    }
    else
    {
        $username = $this->input->post('username', TRUE);
        $this->_in_you_go($username);
    }
}

function test(){
    die($this->input->post('username' , TRUE));
    echo $this->pword_check('test');
}



function pword_check($pword){
        $this->load->library('form_validation');
		$username = $this->input->post('username' , TRUE);
		// $pword = Modules::run('index.php/site_secutiry/make_hash', $pword);
		$pword_hash = md5($pword); 
		$this->load->model('mdl_user');
		$result = $this->mdl_user->pword_check($username, $pword_hash);
                
		if ($result == FALSE)
		{
			$this->form_validation->set_message('pword_check', 'The username and/or password you entered is not correct');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

        
function _in_you_go($username){
	//if successfully logged in creating a sesssion a taking the user to the admin panel
	$query = $this->get_where_custom('username', $username);
	foreach($query->result() as $row){
		$user_id = $row->id;
		$username = $row->username;
        $user_role = $row->user_role;
	}
	$this->session->set_userdata('username', $username);
	$this->session->set_userdata('user_id', $user_id);
    $this->session->set_userdata('user_role', $user_role);
        
	//header('Location:'.base_url().'User_stats/user_stats_list?attr='.$username);
	header('Location:'.base_url().'Dashboard/index?attr='.$user_id);
}


function logout(){
    $this->load->library('session');
    $this->session->sess_destroy();
    $data['view_file'] = "loginform";
    $this->load->module('Main_template');
    $this->main_template->index($data);
}

function userList(){
    Modules::run('Site_security/make_sure_is_admin');
    if($this->session->userdata('user_role') == "su" ){
        $data['query'] =  $this->get_where_user("admin");        
        $data['view_file'] = "userlist";
        $this->load->module('Main_template');
        $this->main_template->index($data);
    }
}

function eventList(){
    Modules::run('Site_security/make_sure_is_admin');
    $data['query'] =  $this->get_events();        
    $data['view_file'] = "eventlist";
    $this->load->module('Main_template');
    $this->main_template->index($data);
}

function get_events(){
    $this->load->model('mdl_user');
    $query = $this->mdl_user->get_events();
    return $query;
}

function activate(){
    Modules::run('Site_security/make_sure_is_admin');
    $data['active'] = 1;
    if($this->session->userdata('user_role') == "su" ){
        $this->_update($_GET['userId'], $data);
        $data['query'] =  $this->get_where_user("admin");        
        $data['view_file'] = "userlist";
        $data['module'] = "User";
        $this->load->module('Main_template');
        $this->main_template->index($data);
    }else{
        die("You are not authorized for this action");
    }
}

function deactivate(){
    Modules::run('Site_security/make_sure_is_admin');
    $data['active'] = 0;
    if($this->session->userdata('user_role') == "su" ){
        $this->_update($_GET['userId'], $data);
        $data['query'] =  $this->get_where_user("admin");        
        $data['view_file'] = "userlist";
        $this->load->module('Main_template');
        $this->main_template->index($data);
    }
}

function activateClientUser(){
    Modules::run('Site_security/make_sure_is_admin');
    $data['active'] = 1;
    if($this->session->userdata('user_role') == "client" ){
        $this->_updateClientUser($_GET['userId'], $data);
        $data['query'] = $this->get_where_custom_app_user("company", $this->session->userdata('user_id'));
        $data['view_file'] = "clientUserlist";
        $this->load->module('Main_template');
        $this->main_template->index($data);
        
    }else{
        die("You are not authorized for this action");
    }
}

function deactivateClientUser(){
    Modules::run('Site_security/make_sure_is_admin');
    $data['active'] = 0;
    if($this->session->userdata('user_role') == "client" ){
        $this->_updateClientUser($_GET['userId'], $data);
        $data['query'] = $this->get_where_custom_app_user("company", $this->session->userdata('user_id'));
        $data['view_file'] = "clientUserlist";
        $this->load->module('Main_template');
        $this->main_template->index($data);
    }
}

function clientUsers(){
    $data['query'] = $this->get_where_custom_app_user("company", $this->session->userdata('user_id'));
    $data['view_file'] = "clientUserlist";
    $this->load->module('Main_template');
    $this->main_template->index($data);

}

function _update($id, $data) {
    $this->load->model('mdl_user');
    $this->mdl_user->_update($id, $data);
}

function _updateClientUser($id, $data) {
    $this->load->model('mdl_user');
    $this->mdl_user->_updateClientUser($id, $data);
}


function get($order_by) {
    $this->load->model('mdl_user');
    $query = $this->mdl_user->get($order_by);
    return $query;
}
function get_where($id) {
    $this->load->model('mdl_user');
    $query = $this->mdl_user->get_where($id);
    return $query;
}

function get_where_user_client($id) {
    $this->load->model('mdl_user');
    $query = $this->mdl_user->get_where_user_client($id);
    return $query;
}

function get_where_user($id) {
    $this->load->model('mdl_user');
    $query = $this->mdl_user->get_where_user($id);
    return $query;
}

function get_where_custom($col, $value) {
    $this->load->model('mdl_user');
    $query = $this->mdl_user->get_where_custom($col, $value);
    return $query;
}


function get_max() {
    $this->load->model('mdl_user');
    $max_id = $this->mdl_user->get_max();
    return $max_id;
}

function register(){
    $data['firstname'] = $_POST['firstname'];
    $data['lastname'] = $_POST['lastname'];
    $data['username'] = $_POST['email'];
    $data['phone'] = $_POST['phone'];
    $data['password'] = md5($_POST['password']);
    $data['user_role'] = 'admin';
    $this->insert_user($data);
    
    // $this->load->library('email');

    //     $this->email->initialize(array(
    //       'protocol' => 'smtp',
    //       'smtp_host' => 'ssl://smtp.gmail.com',
    //       'smtp_user' => 'dedicatedonestoptech@gmail.com',
    //       'smtp_pass' => 'Zeeshan@123',
    //       'smtp_port' => 465,
    //       'crlf' => "\r\n",
    //       'newline' => "\r\n"
    //     ));
    //     $this->email->from('dedicatedonestoptech@gmail.com', 'DigiHymn');
    //     $this->email->to($_POST['email']);
    //     $this->email->subject('Welcome To DigiHymn');
    //     $this->email->message('Hey ' . $data['firstname'] . " " . $data['lastname'] . ", \n Welcome to DigiHymn. This is a confirmation email for your sign up.");
    //     $this->email->send();

    //     echo $this->email->print_debugger();
    //     $this->insert_user_creds($data_user_creds);
    
    
    header('Location:'.base_url());
}

function register_app_user(){
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST');
        
        $data_creds['username'] = $_POST['username'];
        $data_creds['password'] = md5($_POST['password']);
        $data_creds['email'] = $_POST['email'];
        $data['firstname'] = $_POST['firstname'];
        $data['lastname'] = $_POST['lastname'];
        
        $this->load->library('email');

        $this->email->initialize(array(
          'protocol' => 'smtp',
          'smtp_host' => 'ssl://smtp.gmail.com',
          'smtp_user' => 'dedicatedonestoptech@gmail.com',
          'smtp_pass' => 'Zeeshan@123',
          'smtp_port' => 465,
          'crlf' => "\r\n",
          'newline' => "\r\n"
        ));
        $this->email->from('testing@dostechnologies.com', 'DigiHymn');
        $this->email->to($_POST['email']);
        $this->email->subject('Welcome To DigiHymn');
        $this->email->message('Hey ' . $data['firstname'] . " " . $data['lastname'] . ", \n Welcome to DigiHymn. This is a confirmation email for your sign up.");
        $this->email->send();

        echo $this->email->print_debugger();
        
        
        $this->_insert_app_user_creds($data_creds);
        $data['dob'] = $_POST['dob'];
        $data['pcontact'] = $_POST['pcontact'];
        $data['ccontact'] = $_POST['ccontact'];
        
        $data['app_user_id'] = $this->get_max_app_user_id();
        $data['email'] = $_POST['email'];
        $data['church_affiliation'] = $_POST['churchAffiliation'];
        $data['pastor_name'] = $_POST['pastorName'];
        $data['city'] = $_POST['city'];
        $data['state'] = $_POST['state'];
        $data['zip'] = $_POST['zip'];
        $this->_insert_app_user($data);       
        var_dump($data);
        die();
}
function event_add(){
        $data['eventname'] = $_POST['eventname'];
        $data['eventtype'] = $_POST['eventtype'];
        $data['invitor'] = $_POST['invitor'];
        $data['speaker'] = $_POST['speaker'];
        $data['organiser'] = $_POST['organiser'];
        $data['eventdetails'] = $_POST['eventdetails'];
        $data['app_user_id'] = $_POST['app_user_id'];
        $data['is_approved'] = false;
        $this->_insert_event($data); 
        var_dump($data);
        die();
}

function _insert_event($data){
    $this->load->model("mdl_user");
    $this->mdl_user->_insert_event($data);
}

function _insert_app_user($data){
    $this->load->model("mdl_user");
    $this->mdl_user->_insert_app_user($data);
}

function _insert_app_user_creds($data){
    $this->load->model("mdl_user");
    $this->mdl_user->_insert_app_user_creds($data);
}

function get_max_app_user_id() {
    $this->load->model('mdl_user');
    $max_id = $this->mdl_user->get_max_app_user_id();
    return $max_id;
}

function sign_in_app_user(){
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST');
    $data['username'] = $_POST['username'];
    $data['password'] = md5($_POST['password']);
    if($this->check_app_user_creds($data)){
        $query = $this->get_where_custom_app_user('username', $data['username']);
	foreach($query->result() as $row){
            $id = $row->id;
            $username = $row->username;
	}
	$this->session->set_userdata('app_username', $username);
	$this->session->set_userdata('app_user_id', $id);
        echo json_encode($this->session->userdata());
    }else{
        echo 'false';
    }
}

function check_app_user_creds($data){
    $this->load->model("mdl_user");
    return $this->mdl_user->check_app_user_creds($data);
}
function get_where_custom_app_user($col, $value){
    $this->load->model('mdl_user');
    $query = $this->mdl_user->get_where_custom_app_user($col, $value);
    return $query;
}

function get_app_user_data(){
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST');
    $query = $this->get_where_app_user($_GET['id']);
    echo json_encode($query->result());
}

function get_where_app_user($id){
    $this->load->model('mdl_user');
    $query = $this->mdl_user->get_where_app_user($id);
    return $query;
}

function edit_app_user(){
    $data['firstname'] = $_POST['firstname'];
    $data['lastname'] = $_POST['lastname'];
    $data['email'] = $_POST['email'];
    $data['church_affiliation'] = $_POST['churchAffiliation'];
    $data['pastor_name'] = $_POST['pastorName'];
    $data['city'] = $_POST['city'];
    $data['state'] = $_POST['state'];
    $data['zip'] = $_POST['zip'];
    $user_id = $_POST['user_id'];
    
    $this->_update_app_user($user_id, $data);    
}

function _update_app_user($id, $data) {
    $this->load->model('mdl_user');
    $this->mdl_user->_update_app_user($id, $data);
}

function logout_app_user(){
    $this->session->unset_userdata('app_username');
    $this->session->unset_userdata('app_user_id');
}

function checkUsername(){
    if($this->get_where_username($_POST['username'])){
        echo "true";
    }else{
        echo "false";
    }
}

function get_where_username($username){
    $this->load->model('mdl_user');
    return $this->mdl_user->get_where_username($username);
}


function checkAppUsername(){
    if($this->get_where_app_username($_POST['username'])){
        echo "true";
    }else{
        echo "false";
    }
}
function get_where_app_username($username){
    $this->load->model('mdl_user');
    return $this->mdl_user->get_where_app_username($username);
}

function checkAppEmail(){
    if($this->get_where_app_email($_POST['email'])){
        echo "true";
    }else{
        echo "false";
    }
}


function get_where_app_email($email){
    $this->load->model('mdl_user');
    return $this->mdl_user->get_where_app_email($email);
}

function appUserView(){
    $data['query'] = $this->get_where_app_user($_GET['app_id']);
    $data['view_file'] = "appUserView";
    $this->load->module('Main_template');
    $this->main_template->index($data);
}

function updateEvent(){
    $data['is_approved'] = true;
    $this->load->model('mdl_user');
    $this->mdl_user->_update_event($_GET['event_id'], $data);
    $data['query'] =  $this->get_events();        
    $data['view_file'] = "eventlist";
    $this->load->module('Main_template');
    $this->main_template->index($data);
}

function all_events_for_app(){
    $this->load->model('mdl_user');
    $query = $this->mdl_user->all_events_for_app();
    echo json_encode($query->result());
}

function event_details_for_app(){
    $this->load->model('mdl_user');
    $query = $this->mdl_user->event_details_for_app($_POST['event_id']);
    echo json_encode($query->result());
}
function updatePass(){
    $data['password'] = md5($_POST['password']);
    $this->load->model('mdl_user');
    $this->mdl_user->_update_app_user_creds($_POST['id'], $data);
    echo "true";
}

function emailPassword(){
    $password = "digihymn" . rand(400, 1000);
    
    $this->load->library('email');

    $this->email->initialize(array(
      'protocol' => 'smtp',
      'smtp_host' => 'ssl://smtp.gmail.com',
      'smtp_user' => 'dedicatedonestoptech@gmail.com',
      'smtp_pass' => 'Zeeshan@123',
      'smtp_port' => 465,
      'crlf' => "\r\n",
      'newline' => "\r\n"
    ));
    $this->email->from('testing@dostechnologies.com', 'DigiHymn');
    $this->email->to($_POST['email']);
    $this->email->subject('DigiHymn Password');
    $this->email->message("Hey, \n Your temporary password is " . $password . ". You can change in the app later on. \n Keep hymning!" );
    $this->email->send();
    $data['password'] = md5($password);
    $this->load->model('mdl_user');
    $this->mdl_user->_update_app_user_creds($this->mdl_user->get_where_email($_POST['email']), $data);
    echo "true";
}

function addClientUser(){
    $this->load->module("Main_template");
    $data['view_file'] = 'registerform_client';
    $data['module'] = 'User';
    $this->main_template->index($data);
}

function editClientUser(){
    $this->load->module("Main_template");
    $this->load->module("Sites");
    $data['sites'] = $this->sites->get_where_custom("company", $this->session->userdata('user_id'));
    $data['user'] = $this->get_where_user_client($_GET['id']);
    $data['view_file'] = 'edituser_client';
    $data['module'] = 'User';
    $this->main_template->index($data);   
}

function submitClientUser(){
    $data['firstname'] = $_POST['firstname'];
    $data['lastname'] = $_POST['lastname'];
    $data['username'] = $_POST['email'];
    $data['phone'] = $_POST['phone'];
    $data['password'] = md5($_POST['password']);
    $data['active'] = '1';
    $data['client_details'] = '';
    $accessToken = md5($_POST['email'] . $_POST['lastname']);
    $data['access_token'] = $accessToken;
    $data['company'] = $this->session->userdata("user_id");
    $this->insert_client_user($data);
    $data['query'] = $this->get_where_custom_app_user("company", $this->session->userdata('user_id'));
    $data['view_file'] = "clientUserlist";
    $this->load->module('Main_template');
    $this->main_template->index($data);
}

function editClientUserSubmit(){
    $data_user['firstname'] = $_POST['firstname'];
    $data_user['lastname'] = $_POST['lastname'];
    $data_user['phone'] = $_POST['phone'];
    // $data_user['password'] = md5($_POST['password']);
    $data_user['active'] = '1';
    if(isset($_POST['sites'])){
        $this->deleteUserSites($_POST['userId']);
        foreach ($_POST['sites'] as $row) {
            $data['user_id'] = $_POST['userId'];
            $data['site_id'] = $row;
            $data_camera['user_id'] = $_POST['userId'];
            $this->insertUserSite($data);
        }
    }    
    $this->_updateClientUser($_POST['userId'] ,$data_user);
    $data['query'] = $this->get_where_custom_app_user("company", $this->session->userdata('user_id'));
    $data['view_file'] = "clientUserlist";
    $this->load->module('Main_template');
    $this->main_template->index($data);
}

function insertUserSite($data){
    $this->load->model("mdl_user");
    $this->mdl_user->insertUserSite($data);
}

function deleteUserSites($id){
    $this->load->model("mdl_user");
    $this->mdl_user->deleteUserSite($id);
}

function submitUser(){
    $data_client['firstname'] = $_POST['firstname'];
    $data_client['lastname'] = $_POST['lastname'];
    $data_client['user_role'] = "admin";
    $data_client['active'] = 1;
    $data_client['phone'] = $_POST['phone'];
    $data_client['username'] = $_POST['email'];
    $data_client['password'] = md5($_POST['password']);
    $this->_insertUser($data_client);
    $this->userList();
}

function _insertUser($data){
    $this->load->model("mdl_user");
    $this->mdl_user->_insertUser($data);
}


}
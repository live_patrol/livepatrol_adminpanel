<style>
  .loading{
    background-image: url("../assets/images/default.gif");
    background-repeat: no-repeat;
      background-attachment: fixed;
      background-position: center; 
  }
  .opa{
    opacity : 0;
  }
</style>
<div class="text-center" style="padding:50px 0" id="to-hide">
   <div class="logo">edit client</div>
   <!-- Main Form -->
   <div class="">
      <div class="main-login-form">
         <?php foreach ($client_data->result() as $row){ ?>
         <input type="hidden" id="user_id" value="<?php echo $_SESSION['user_id']?>" />
         <div class="container-fluid">
            <form method="post" id ="editClient" name = "editClient">
               <div class="form-group col-lg-8">
                  <input type ="hidden" id="client_id" name ="client_id" value ="<?php echo $row->id; ?>" />
                  <label class ="pull-left">Client Name</label>
                  <input type="text" class="form-control" id="name" name="firstname" placeholder="company name" value ="<?php echo $row->firstname; ?>" required />
               </div>
               <div class="form-group col-lg-4">
                  <input type ="hidden" id="client_id" name ="client_id" value ="<?php echo $row->id; ?>" />
                  <label class ="pull-left">CLient Phone</label>
                  <input type="text" class="form-control" id="phone" name="phone" placeholder="phone" value ="<?php echo $row->phone; ?>" required />
               </div>
               <div class="form-group col-lg-12">
                  <label class ="pull-left">Password</label>
                  <input type="text" class="form-control" id="password" name="password" placeholder="password" value ="<?php echo $row->password; ?>" required />
               </div>
               <!-- <div class="form-group col-lg-6">
                  <label class ="pull-left">Username</label>
                  <input type="text" class="form-control" id="username" name="username" placeholder="username" value ="<?php echo $row->username; ?>" required />
               </div>
               <div class="form-group col-lg-6">
                  <label class ="pull-left">Password</label>
                  <input type="text" class="form-control" id="password" name="password" placeholder="password" value ="<?php echo $row->password; ?>" required />
               </div>
               <div class="form-group col-lg-12">
                  <label class ="pull-left">Client Details</label>
                  <textarea class="form-control" id="client_details" name="client_details" placeholder="client details" required><?php echo $row->client_details; ?></textarea>
               </div> -->
               <div class ="row">
                  <div class ="pull-right" style = "margin-bottom : 10px">
                     <span type ="submit" class = "btn btn-default edit-client" style="width:80%!important; margin-right:100px">Edit</span>
                  </div>
               </div>
            </form>
            <!-- <div id="siteDiv">
              <?php 
                $siteCounter = 0;
                if(!empty($site_data)){
                  foreach($site_data->result() as $row1){
                    $siteCounter++; ?>
                    <span class="fa fa-2x pull-right fa-minus"></span>
                    <div class="container-fluid">
                        <input type="hidden" class ="site_id" name="site_id" value="<?php echo $row1->id; ?>"  />
                        <input type="hidden" id="client_id" name="client_id" value="<?php echo $row->id; ?>"  />
                        <div class="form-group col-lg-12">
                            <label class ="pull-left">Site Name</label>
                            <input class="form-control" type="text" id="site_name" name="site_name" value="<?php echo $row1->site_name; ?>" />
                        </div>
                        <?php 
                          if(!empty($camera_data[$row1->id])){
                            $cameraCounter = 0;
                              foreach($camera_data[$row1->id]->result() as $row2){
                                $cameraCounter++; ?>
                                <h3>camera <?php echo $cameraCounter; ?></h3>
                                <form>
                                <div class="container-fluid">
                                  <div class="form-group col-lg-12">
                                   <input type="hidden" class ="camera_id" name="camera_id" value="<?php echo $row2->id; ?>"  />
                                   <input type="hidden" id="client_id" name="client_id" value="<?php echo $row->id; ?>"  />
                                   <div class="form-group col-lg-6">
                                      <label class ="pull-left">RTSP URL</label>
                                      <input class="form-control" type="text" id="rtsp_url<?php echo $siteCounter . $cameraCounter; ?>" name="rtsp_url" value="<?php echo $row2->rtsp_url; ?>" />
                                   </div>
                                   <div class="form-group col-lg-6">
                                      <label class ="pull-left">RTSP PORT</label>
                                      <input type="text" class="form-control" id="rtsp_port<?php echo $siteCounter . $cameraCounter; ?>" name="rtsp_port" value="<?php echo $row2->rtsp_port; ?>" />
                                   </div>
                                   <div class="form-group col-lg-6">
                                      <label class ="pull-left">Camera Username</label>
                                      <input type="text" class="form-control" id="camera_username<?php echo $siteCounter . $cameraCounter; ?>" name="camera_username" value="<?php echo $row2->camera_username; ?>" />
                                   </div>
                                   <div class="form-group col-lg-6">
                                      <label class ="pull-left">Camera Password</label>
                                      <input type="text" class="form-control" id="camera_password<?php echo $siteCounter . $cameraCounter; ?>" name="camera_password" value="<?php echo $row2->camera_password; ?>" />
                                   </div>
                                   <div class = "col-lg-12"><span class = "btn btn-default" onclick = "testCamera(this, '+siteCounter+cameraCounter+')" >Test Camera URL</span></div>

                                   <div class="pull-right" style="margin-bottom : 20px"> 
                                        <span style="width:100%!important" class="btn btn-danger delete-camera">Delete</span> 
                                    </div>
                                   <div class ="pull-right" style = "margin-bottom : 20px">
                                      <span style="width:100%!important" id="edit-btn<?php echo $siteCounter . $cameraCounter; ?>" class = "btn btn-default edit-camera" disabled="true" >Edit</span>
                                   </div>
                                  </div>
                                </div>
                                </form>

                            <?php  }
                          }
                        ?>
                        <div id ="newCameraDiv<?php echo $siteCounter; ?>" >
                        </div>
                        <span id = "newCamera" class="btn btn-default newCamera"  camera = "<?php echo $cameraCounter; ?>" site = "<?php echo $siteCounter; ?>" style = "width:100%; margin-bottom:20px">add camera</span><br>
                        <div class ="" style = "margin-bottom : 20px">
                            <span style="width:80%!important" site-id="<?php echo $row1->id; ?>" id="delete-btn<?php echo $siteCounter ?>" class = "btn btn-danger delete-site" onclick="deleteSite(this)" >Delete Site</span>
                         </div>
                    </div>  
                  <?php }
                }
              ?>
            </div> -->
            <!-- <div id ="newCameraDiv" >
            </div>
            <span id = "newCamera" class="btn btn-default newCamera" style = "width:100%">add camera</span><br> -->
            <!-- <div id ="newSiteDiv" >
            </div>
            <span id = "newSite" class="btn btn-default newSite" style = "width:100%">add site</span><br> -->
         </div>
         <br><br>
         <?php } ?>    
      </div>
   </div>
</div>

<script>
$(document).ready(function(){
        var siteCounter = <?php echo $siteCounter; ?>;
        var cameraCounter = 0;
        $(document).on("click" , ".btn" , function(){
                if($(this).hasClass("edit-client")){
                    var serializedForm = $(this).parents("form").serialize();
                    $.ajax({
                        type: 'POST',
                        url: "<?php echo base_url(); ?>Client/editClient?attr="+$("#user_id").val(),
                        data: serializedForm,
                        success: function(response) {
                           alert(response)
                        },
                        error: function(){
                            alert("internal error");
                        }
                    });
                }
                if($(this).hasClass("edit-camera")){
                    if($(this).attr("disabled") == "disabled"){
                      alert("Test the url first");
                    }else{
                      var serializedForm = $(this).parents("form").serialize();
                      // alert(serializedForm);
                      $.ajax({
                          type: 'POST',
                          url: '<?php echo base_url(); ?>Client/editCamera?attr='+$("#user_id").val(),
                          data: serializedForm,
                          success: function(response) {
                             alert(response)
                          },
                          error: function(){
                              alert("internal error");
                          }
                      });
                    }
                }
                var site = $(this).attr("site");
                if($(this).hasClass("newCamera")){
                    cameraCounter++;
                    var clientId = $("#client_id").val();
                    var col = '<h3>camera '+cameraCounter+'</h3><span class="fa fa-2x pull-right fa-minus"></span><div class="form-group col-lg-12"><input type="hidden" name="cameraCounter'+siteCounter+'" value="'+cameraCounter+'"><div class="form-group col-lg-8"><label>Camera Public URL</label><input type="text" class="form-control" id="rtsp_url'+siteCounter+cameraCounter+'" name="rtsp_url'+siteCounter+cameraCounter+'" placeholder="camera rtsp url" required /></div><div class="form-group col-lg-4"><label>Camera RTSP Port</label><input type="text" class="form-control" id="rtsp_port'+siteCounter+cameraCounter+'" name="rtsp_port'+siteCounter+cameraCounter+'" placeholder="camera rtsp port" required /></div><div class="form-group col-lg-6"><label>Camera Username</label><input type="text" class="form-control" id="camera_username'+siteCounter+cameraCounter+'" name="camera_username'+siteCounter+cameraCounter+'" placeholder="camera Username" required /></div><div class="form-group col-lg-6"><label>Camera RTSP Password</label><input type="text" class="form-control" id="camera_password'+siteCounter+cameraCounter+'" name="camera_password'+siteCounter+cameraCounter+'" placeholder="camera rtsp port" required /></div><div class = "col-lg-12"><span class = "btn btn-default" onclick = "testCamera(this, '+siteCounter+cameraCounter+')" >Test Camera URL</span></div></div><br><br>';
                    $("#newCameraDiv"+site).append(col);
                    $("#addCamera").attr("disabled" , "true");
                }else if($(this).hasClass("newSite")){
                    cameraCounter = 0;
                    siteCounter++;
                    var clientId = $("#client_id").val();
                    var col = '<h3>Site '+siteCounter+'</h3><form id="site'+siteCounter+'" method="post"><input type="hidden" name="siteCounter" value="'+siteCounter+'"><input type="hidden" name="clientId" value="'+clientId+'"><span class="fa fa-2x pull-right fa-minus"></span><div class="container-fluid"><div class="form-group col-lg-12"><div class="form-group col-lg-12"><label>Sitename</label><input type="text" class="form-control" id="site_name'+siteCounter+'" name="site_name'+siteCounter+'" placeholder="Sitename" required /></div></div><div id = "newCameraDiv'+siteCounter+'" class = "container-fluid"></div><span id = "newCamera" class="btn btn-default newCamera" style = "width:30%!important; margin-bottom:20px" site = "'+siteCounter+'" camera = "0" >add camera</span><br><span class="btn btn-default" style = "width:100%" site = "'+siteCounter+'" onclick="submitSite(this)" >submit site</span><br></div><br><br></form>';
                    $("#newSiteDiv").append(col);
                }

                if($(this).hasClass("add-camera")){
                    var serializedForm = $(this).parents("form").serialize();
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo base_url(); ?>Camera/addCamera?attr='+$("#user_id").val(),
                        data: serializedForm,
                        success: function(response) {
                           location.reload();
                        },
                        error: function(){
                            alert("internal error");
                        }
                    });
                }
                if($(this).hasClass("delete-camera")){
                    var cameraId = $(this).parents("form").find(".camera_id").val();
                    $.ajax({
                        type: 'GET',
                        url: '<?php echo base_url(); ?>Camera/deleteCamera?attr='+$("#user_id").val()+'&&id='+cameraId,
                        success: function(response) {
                           location.reload();
                        },
                        error: function(){
                            alert("internal error");
                        }
                    });
                }
        });
});


function testCamera(el, count){
      // alert("URL : " + $("#rtsp_url"+count).val());
      // alert("Username : " + $("#camera_username"+count).val());
      // alert("Password : " + $("#camera_password"+count).val());
      // alert("Port : " + $("#rtsp_port"+count).val());
      
      // alert($("#rtsp_port"+count).val());
      $("body").addClass("loading");
      $("#to-hide").addClass("opa");
      // $("#addCamera").attr("disabled" , "true");
    $.ajax({
        url: 'http://54.190.254.95:8080/checkCameraUrl',
        type: 'post',
        data: JSON.stringify({"cameraHost":$("#rtsp_url"+count).val(), "cameraUsername":$("#camera_username"+count).val(), "cameraPassword":$("#camera_password"+count).val(), "cameraPort":$("#rtsp_port"+count).val()}),
        contentType: "application/json;charset=utf-8",
        dataType: 'json',
        success: function (data) {
            $("body").removeClass("loading");
          $("#to-hide").removeClass("opa");
            if(data.status != "0"){
              var str = data.data.uri;
              var splited = str.split(":");
              var replaced = str.replace(splited[1], "//"+ $('#camera_username'+count).val() + ":" + $('#camera_password'+count).val() +"@" + $('#rtsp_url'+count).val());
              $("#rtsp_url"+count).val(replaced);
              alert("Camera Url Correct!");
              $("#rtsp_url"+count).addClass(".border-green");
              $("#camera_username"+count).addClass("disabled");
              $("#camera_password"+count).addClass("disabled");
              $("#rtsp_port"+count).addClass("disabled");
              $(".btn").removeAttr("disabled");
              if($(this).attr("id") == "edit-btn"+count){
                $("#edit-btn"+count).removeAttr("disabled");
              }else{
                $(".edit-camera").attr("disabled", "true");
              }
            }else if (data.status == "0") {
              alert(data.error_msg);
              $("#rtsp_url"+count).val("");
              $("#rtsp_username"+count).val("");
              $("#rtsp_password"+count).val("");
              $("#rtsp_port"+count).val("");

            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
          $("body").removeClass("loading");
          $("#to-hide").removeClass("opa");
              alert("Status: " + textStatus); alert("Error: " + errorThrown); 
              $("#addCamera").attr("disabled" , "true");
          }
      });
    }

function submitSite(ele){
    var ser = $("#site"+$(ele).attr("site")).serialize();
    var clientId = $("#client_id").val();
    console.log(ser);
    $("body").addClass("loading");
    $("#to-hide").addClass("opa");


    $.ajax({
          url: '<?php echo base_url(); ?>Sites/addSite?attr='+clientId,
          type: 'post',
          data: ser,
          success: function (data) {
            if(data=="okay"){
              location.reload();
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
          $("body").removeClass("loading");
          $("#to-hide").removeClass("opa");
              alert("Status: " + textStatus); alert("Error: " + errorThrown); 
              $("#addCamera").attr("disabled" , "true");
          }
      });
}

function deleteSite(el){
  var siteId = $(el).attr("site-id");
  var clientId = $("#client_id").val();
  $("body").addClass("loading");
  $("#to-hide").addClass("opa");
  $.ajax({
        url: '<?php echo base_url(); ?>Sites/deleteSite?attr='+clientId+'&id='+siteId,
        success: function (data) {
          if(data=="okay"){
            location.reload();
          }
      },  
      error: function(XMLHttpRequest, textStatus, errorThrown) { 
        $("body").removeClass("loading");
        $("#to-hide").removeClass("opa");
            alert("Status: " + textStatus); alert("Error: " + errorThrown); 
            $("#addCamera").attr("disabled" , "true");
        }
    });
}



                
</script>




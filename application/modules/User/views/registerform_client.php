<!-- REGISTRATION FORM -->
<div class="text-center" style="padding:50px 0">
	<div class="logo">register</div>
	<!-- Main Form -->
	<div class="login-form-1">
            <form id="register-form" action ="<?php echo base_url(); ?>User/submitClientUser?attr=<?php echo $_SESSION['user_id']; ?>"  method ="post" class="text-left">
			<div class="login-form-main-message"></div>
			<div class="main-login-form">
				<div class="login-group">
                	<div class="form-group">
                        <label for="firstname" class="sr-only">First Name</label>
                        <input type="text" class="form-control" id="firstname" name="firstname" placeholder="first name" required />
					</div>
                    <div class="form-group">
                        <label for="lastname" class="sr-only">First Name</label>
                        <input type="text" class="form-control" id="lastname" name="lastname" placeholder="last name" required />
					</div>
					<div class="form-group">
                        <input type="email" class="form-control" id="email" name="email" placeholder="email/username" required />
                        <span style ="color:red" id ="user_check" ></span>
					</div>
                    <div class="form-group">
                        <label for="phone" class="sr-only">Phone Number</label>
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="enter phone number" required />
					</div>
					<div class="form-group">
                        <input name="password" id="password" type="password" name ="form-control" size="15" maxlength="20"                                             <input name="password" id="password" type="password" name ="form-control" size="15" maxlength="20" required />
                        <span id="strength">Type Password</span>
					</div>
                        <div class="form-group">
                            <label for="reg_password" class="sr-only">Retype Password</label>
                            <input type="password" class="form-control" id="re_password" placeholder="retype password" required />
					</div>
<!--					<div class="form-group login-group-checkbox">
						<input type="checkbox" class="" id="reg_agree" name="reg_agree">
						<label for="reg_agree">i agree with <a href="#">terms</a></label>
					</div>-->
				</div>
                            <button type="submit" id ='sub-btn' class="login-button" disabled="true" ><i class="fa fa-chevron-right"></i></button>
			</div>
			<!-- <div class="etc-login-form">
                            <p>already have an account? <a href="<?php echo base_url(); ?>">login here</a></p>
			</div> -->
		</form>
	</div>
	<!-- end:Main Form -->
</div>
<script language="javascript">
    $(document).ready(function(){
       $("#password").keyup(function(){
            var strength = $("#strength");
            var strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\W).*$", "g");
            var mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
            var enoughRegex = new RegExp("(?=.{6,}).*", "g");
            var pwd = $("#password");
            if (pwd.val().length==0) {
                strength.html("Type Password") ;
            } else if (false == enoughRegex.test(pwd.val())) {
                strength.html("More Characters");
            } else if (strongRegex.test(pwd.val())) {
                strength.html('<span style="color:green">Strong!</span>');
            } else if (mediumRegex.test(pwd.val())) {
                strength.html('<span style="color:orange">Medium!</span>');
            } else {
            strength.html('<span style="color:red">Weak!</span>');
            }
        }); 
        $("#re_password").keyup(function(){
            var pass = $("#password").val();
            var rePass = $("#re_password").val();
            if(rePass !== pass){
                $(this).removeClass("green-border");
                $(this).addClass("red-border");
            }else{
                $(this).removeClass("red-border");
                $(this).addClass("green-border");
                $("#sub-btn").prop("disabled", false);
            }
        });
        
        $("#email").focusout(function(){
            var obj = {
                username : $("#email").val()
            }
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url(); ?>User/checkUsername',
                data: obj,
                success: function(response) {
                   if(response == "true"){
                       $("#email").val("");
                       $("#user_check").html("Username already exists. Please choose a different one.");
                   }else{
                       
                   }
                   
                },
                error: function(){
                    alert("internal error");
                }
            });
        });
        
    });
</script>

<div id="login-div" class="text-center" style="padding:125px 0; margin-top:-40px">
	<div class="logo">login</div>
	<!-- Main Form -->
	<div class="login-form-1">
            <?php
                echo validation_errors('<p style ="color : red;" >' , '</p>');
            ?>
            <form id="login-form" method ='post' action="<?php echo base_url()?>User/submit" class="text-left">
			<div class="login-form-main-message"></div>
			<div class="main-login-form">
				<div class="login-group">
					<div class="form-group">
                                            <input type="text" class="form-control" id="username" name="username" placeholder="username">
					</div>
					<div class="form-group">
                                            <input type="password" class="form-control" id="pword" name="pword" placeholder="password">
					</div>
<!--					<div class="form-group login-group-checkbox">
                                            <input type="checkbox" id="lg_remember" name="lg_remember">
                                            <label for="lg_remember">remember</label>
					</div>-->
				</div>
				<button type="submit" class="login-button"><i class="fa fa-chevron-right"></i></button>
			</div>
			<div class="etc-login-form">
				<div class = "row">
					<div class="col-lg-6"></div>
					<div class="col-lg-6" style="margin-top:30px">
						<img src = "<?php echo base_url(); ?>assets/images/logo.png">
					</div>
				</div>
				
				<!-- <p>forgot your password? <a href="#">click here</a></p> -->
				
					<!-- <p style="color:#67A848!important">new user? <a href="<?php echo base_url(); ?>User/registerform">create new account</a></p> -->
			</div>
		</form>
                
	</div>
	<!-- end:Main Form -->
</div>

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_dashboard extends CI_Model {

function __construct() {
parent::__construct();
}

function get_table() {
    $table = "";
    return $table;
}

function get($order_by) {
    $table = $this->get_table();
    $this->db->order_by($order_by);
    $query=$this->db->get($table);
    return $query;
}


function get_where($id) {
    $table = 'livepatrol_user';
    $this->db->where('username', $id);
    $query=$this->db->get($table);
    return $query;
}

function get_max() {
    $table = $this->get_table();
    $this->db->select_max('id');
    $query = $this->db->get($table);
    $row=$query->row();
    $id=$row->id;
    return $id;
}

function get_where_custom($col, $value) {
    $table = 'user_creds';
    $this->db->where($col, $value);
    $query=$this->db->get($table);
    return $query;
}

function _update($id, $data) {
    $table = 'users';
    $this->db->where('id', $id);
    $this->db->update($table, $data);
}
function _update_creds($id, $data) {
    $table = 'user_creds';
    $this->db->where('user_id', $id);
    $this->db->update($table, $data);
}
}
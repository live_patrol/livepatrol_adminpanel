<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Client extends MX_Controller
{

function __construct() {
parent::__construct();
// Modules::run('Site_security/make_sure_is_not_client');
}
function index(){
    Modules::run('Site_security/make_sure_is_admin');
	$data['query'] = $this->get("id");
    $this->load->module("Main_template");
    $data['view_file'] = 'clients_view';
    $data['module'] = 'Client';
    $this->main_template->index($data);
}

// function testCamera(){
//     echo $_POST['cameraHost'];
// }

function add(){
    Modules::run('Site_security/make_sure_is_admin');
    $this->load->module("Main_template");
    $data['view_file'] = 'addclient_view';
    $data['module'] = 'Client';
    $this->main_template->index($data);
}

function edit(){
    Modules::run('Site_security/make_sure_is_admin');
    $data['client_data'] = $this->get_where($_GET['id']);
    // $this->load->module('Camera');
    // $this->load->module('Sites');
    // $data['site_data'] = $this->sites->get_site_where_user($_GET['id']);
    // if(!empty($data['site_data']->result())){
    //     foreach ($data['site_data']->result() as $row) {
    //         $data['camera_data'][$row->id] = $this->camera->get_camera_where_site($row->id);
    //     }
    // }
    $this->load->module("Main_template");
    $data['view_file'] = 'editclient_view';
    $data['module'] = 'Client';
    $this->main_template->index($data);
}


function editClient(){
    Modules::run('Site_security/make_sure_is_admin');
    $data_client['firstname'] = $_POST['firstname'];
    $data_client['phone'] = $_POST['phone'];
    $data_client['password'] = md5($_POST['password']);
    $client_id = $_POST['client_id'];
    $this->_update($client_id ,$data_client);
    echo "Success";
}


function editCamera(){
    Modules::run('Site_security/make_sure_is_admin');
    $data_camera['rtsp_url'] =  $_POST['rtsp_url'];
    $data_camera['rtsp_port'] =  $_POST['rtsp_port'];
    $data_camera['camera_username'] =  $_POST['camera_username'];
    $data_camera['camera_password'] =  $_POST['camera_password'];
    $id = $_POST['camera_id'];
    $this->load->module('Camera');
    $this->camera->_update($id ,$data_camera);
    echo "success";
}

function submitClient(){
    $data_client['firstname'] = $_POST['companyname'];
    $data_client['lastname'] = "";
    $data_client['user_role'] = "client";
    $data_client['active'] = 1;
    $data_client['phone'] = $_POST['phone'];
    $data_client['username'] = $_POST['username'];
    $data_client['password'] = md5($_POST['password']);
    $this->_insert($data_client);

    // $accessToken = md5($_POST['username'] . $_POST['lastname']);
    // $data_client['access_token'] = $accessToken;
    // $data_client['password'] = md5($_POST['password']);
    // $data_client['client_details'] = $_POST['client_details'];
    // $this->_insert($data_client);
    // if(isset($_POST['siteCounter'])){
    //     $siteCounter = $_POST['siteCounter'];
    //     for ($x = 1; $x <= $siteCounter ; $x++) {
    //         $data_site['site_name'] = $_POST['site_name'.$x];
    //         $data_site['user_id'] = $this->get_max();
    //         $this->load->module('Sites');
    //         $this->sites->_insert($data_site);
    //         if(isset($_POST['cameraCounter' . $x])){
    //             $cameraCounter = $_POST['cameraCounter' . $x];
    //             for ($i = 1; $i <= $cameraCounter ; $i++) {
    //                     $data_camera['rtsp_url'] =  $_POST['rtsp_url'.$x.$i];
    //                     $data_camera['rtsp_port'] =  $_POST['rtsp_port'.$x.$i];
    //                     $data_camera['camera_username'] =  $_POST['camera_username' .$x.$i];
    //                     $data_camera['camera_password'] =  $_POST['camera_password' .$x.$i];
    //                     $data_camera['user_id'] = $this->get_max();
    //                     $data_camera['site_id'] = $this->sites->get_max();
    //                     $this->load->module('Camera');
    //                     $this->camera->_insert($data_camera);
    //             }
    //         }
    //     }
    // }
    // if(isset($_POST['cameraCounter'])){
    //     $cameraCounter = $_POST['cameraCounter'];
    //     for ($x = 1; $x <= $cameraCounter ; $x++) {
    //             $data_camera['rtsp_url'] =  $_POST['rtsp_url'.$x];
    //             $data_camera['rtsp_port'] =  $_POST['rtsp_port'.$x];
    //             $data_camera['camera_username'] =  $_POST['camera_username' .$x];
    //             $data_camera['camera_password'] =  $_POST['camera_password' .$x];

                
    //             $data_camera['user_id'] = $this->get_max();

    //             $this->load->module('Camera');
    //             $this->camera->_insert($data_camera);
    //     }
    // }
    // $this->db->trans_complete();
    $this->index();
}

function delete(){
    $clientId = $_GET['id'];
    $this->load->module('Camera');
    $this->load->module('Sites');
    // $this->camera->_delete($clientId);
    $this->sites->_delete($clientId);
    $this->_delete($clientId);
    $this->index();   
}


function checkUsername(){
    if($this->get_where_username($_POST['username'])){
        echo "true";
    }else{
        echo "false";
    }
}

function get_where_username($username){
    $this->load->model('Mdl_client');
    return $this->Mdl_client->get_where_username($username);
}


function get($order_by) {
$this->load->model('Mdl_client');
$query = $this->Mdl_client->get($order_by);
return $query;
}

function get_with_limit($limit, $offset, $order_by) {
$this->load->model('Mdl_client');
$query = $this->Mdl_client->get_with_limit($limit, $offset, $order_by);
return $query;
}

function get_where($id) {
$this->load->model('Mdl_client');
$query = $this->Mdl_client->get_where($id);
return $query;
}

function get_where_custom($col, $value) {
$this->load->model('Mdl_client');
$query = $this->Mdl_client->get_where_custom($col, $value);
return $query;
}

function _insert($data) {
$this->load->model('Mdl_client');
$this->Mdl_client->_insert($data);
}

function _update($id, $data) {
$this->load->model('Mdl_client');
$this->Mdl_client->_update($id, $data);
}

function _delete($id) {
$this->load->model('Mdl_client');
$this->Mdl_client->_delete($id);
}

function count_where($column, $value) {
$this->load->model('Mdl_client');
$count = $this->Mdl_client->count_where($column, $value);
return $count;
}

function get_max() {
$this->load->model('Mdl_client');
$max_id = $this->Mdl_client->get_max();
return $max_id;
}

function _custom_query($mysql_query) {
$this->load->model('Mdl_client');
$query = $this->Mdl_client->_custom_query($mysql_query);
return $query;
}

}